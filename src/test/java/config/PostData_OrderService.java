package config;

import com.codoid.products.exception.FilloException;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PostData_OrderService {

    final static Logger log = Logger.getLogger(MySqLQueries.class.getName());

    static String filepath = "data/OrderService.xls";
    static  ExcelUtils utils=new ExcelUtils(filepath);


    public static JSONObject add_to_cart_l2(String scenario) throws FilloException{
        JSONObject main_data=new JSONObject();
        main_data.put("product_id", utils.get_data("create_order",scenario, "product_id"));
        main_data.put("quantity", utils.get_data("create_order", scenario, "quantity"));
        main_data.put("hub_id", utils.get_data("create_order", scenario, "hub_id"));
        main_data.put("customer_key", utils.get_data("create_order",scenario, "customer_key"));
        main_data.put("source", utils.get_data("create_order",scenario, "source"));
        return main_data;
    }
    public static JSONObject add_to_cart_l2MultipleProducts(String scenario,int no_of_items) throws FilloException{
        JSONObject main_data=new JSONObject();
        main_data.put("product_id", utils.get_data("create_order", "TC"+no_of_items, "product_id"));
        main_data.put("quantity", utils.get_data("create_order", "TC"+no_of_items, "quantity"));
        main_data.put("hub_id", utils.get_data("create_order", "TC1", "hub_id"));
        main_data.put("customer_key", utils.get_data("create_order", "TC1", "customer_key"));
        main_data.put("source", utils.get_data("create_order", "TC1", "source"));
        return main_data;
    }


    public static JSONObject create_order_l2() throws JSONException, FilloException{
        JSONObject main_data=new JSONObject();
        JSONArray ar = new JSONArray();
        JSONObject sub_data =  null;
        sub_data=new JSONObject();
        sub_data.put("type", "COD");
        sub_data.put("amount_to_deduct", utils.get_data("create_order", "TC1", "amount_to_deduct"));
        ar.put(sub_data);
        main_data.put("payments", ar);
        main_data.put("address_id", utils.get_data("create_order", "TC1", "address_id"));
        main_data.put("customer_key",utils.get_data("create_order", "TC1", "customer_key"));
        main_data.put("coupon","");
        main_data.put("hub_id", utils.get_data("create_order", "TC1", "hub_id"));
        main_data.put("source", "website");
        main_data.put("total", utils.get_data("create_order", "TC1", "total"));
        return main_data;
    }

    public static JSONObject reschedule(String date,String scenario ,int start, int end) throws JSONException, FilloException{
        JSONObject main_data=new JSONObject();
        List<String> list=new ArrayList<>();
        for(; start<=end; start++) {
            String data = utils.get_data("Orderservice", scenario, "Slot_"+start);
            list.add(data);
        }
        Iterator<String> iterato = list.iterator();
        main_data.put("schedule_date", date);
        while(iterato.hasNext()) {
            String currentSlot = iterato.next();
            if(currentSlot != null && ! currentSlot.isEmpty()) {
                main_data.put("schedule_time", currentSlot);
                break;
            }
        }
        main_data.put("by_user", "soumya");
        main_data.put("is_override", false);
        main_data.put("order_id",utils.get_data("order", "hub_details", "order_id"));
        return main_data;
    }

    public static JSONObject remove_Cart_Item_l2(String scenario) throws FilloException{
        JSONObject main_data=new JSONObject();
        main_data.put("product_id", utils.get_data("create_order", scenario, "product_id"));
        main_data.put("hub_id", utils.get_data("create_order", scenario, "hub_id"));
        main_data.put("customer_key", utils.get_data("create_order", scenario, "customer_key"));
        return main_data;
    }

    public static JSONObject reject_L2() throws JSONException, FilloException{
        JSONObject main_data=new JSONObject();
        main_data.put("reject_reason", "testing");
        main_data.put("by_user", "soumya");
        main_data.put("is_override", false);
        main_data.put("order_id",utils.get_data("order", "hub_details", "order_id"));
        return main_data;
    }
    public static JSONObject cancel_order() throws JSONException, FilloException{
        JSONObject main_data=new JSONObject();
        main_data.put("reject_reason", "User rejected");
        main_data.put("entity_id", utils.get_data("onboard", "onboard", "entity_id"));
        main_data.put("order_id",utils.get_data("generate_barcode", "generate_barcode", "order_id"));
        return main_data;
    }

    public static JSONObject edit_Order_l2() throws JSONException, FilloException{
        JSONObject main_data=new JSONObject();
        JSONArray ar = new JSONArray();
        JSONObject sub_data =  null;
        sub_data=new JSONObject();
        sub_data.put("product_id", utils.get_data("create_order", "TC1", "edit_product_id"));
        sub_data.put("quantity", utils.get_data("create_order", "TC1", "edit_pr_qty"));
        sub_data.put("reason",utils.get_data("create_order", "TC1", "reason"));
        ar.put(sub_data);
        main_data.put("products", ar);
        main_data.put("order_id", utils.get_data("create_order", "TC1", "fetch_order_id"));
        return main_data;
    }
}
