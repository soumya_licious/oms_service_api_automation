package config;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import java.util.ArrayList;
import java.util.List;

public class ExcelUtils {

    String filepath = "";

    public ExcelUtils(String fielpath){
        this.filepath = fielpath;
    }

    public String get_data(String sheet, String scenario, String fieldValue) throws FilloException {
        Fillo fillo = new Fillo();
        Connection connection = fillo.getConnection(filepath);
        String strQuery = "select * from " + sheet + " where id =" + "'" + scenario + "'";
        Recordset rs = connection.executeQuery(strQuery);
        while (rs.next())
        {
            return rs.getField(fieldValue);
        }

        rs.close();
        connection.close();
        return rs.getField(fieldValue);

    }



    public String get_data_SearchUpdate(String sheet, String scenario, String fieldValue) throws FilloException {
        Fillo fillo = new Fillo();
        Connection connection = fillo.getConnection(filepath);
        String strQuery = "select * from " + sheet + " where rowId=" + "'" + scenario + "'";
        Recordset rs = connection.executeQuery(strQuery);
        while (rs.next()) {
            return rs.getField(fieldValue);
        }
        return rs.getField(fieldValue);
    }

    public void insert_data(String sheet, String data) throws FilloException {
        Fillo fillo = new Fillo();
        Connection connection = fillo.getConnection(filepath);
        String strQuery = "insert into " + sheet + "(id, serailnumber) values('newuser', '" + data + "')";
        connection.executeUpdate(strQuery);
        connection.close();
    }

    public void gpu_update_data(String sheet, String data, String scenario) throws FilloException {
        Fillo fillo = new Fillo();
        Connection connection = fillo.getConnection(filepath);
        String strQuery = "update " + sheet + " set serailnumber='" + data + "' where id=" + "'" + scenario + "'";
        connection.executeUpdate(strQuery);
        connection.close();
    }


    public List<String> store_all_rows_into_list(String filepath, String sheet, String data) throws FilloException {
        List<String> Testlist = new ArrayList<String>();
        Fillo fillo = new Fillo();
        Connection connection = fillo.getConnection(filepath);
        System.out.println("Sheet "+sheet);
        String strQuery = "select * from " + sheet + "";
        Recordset recordset = connection.executeQuery(strQuery);
        while (recordset.next()) {
            Testlist.add(recordset.getField(data));
        }
        recordset.close();
        connection.close();
        return Testlist;
    }
    /**
     * This method helps to update the data on to excel .where in the scenario is made constant so that the data, based on the 
     * field value provided will update accordingly. sheet name of the excel to be mentioned.
     * @param sheet
     * @param data
     * @param fieldvalue
     * @param scenario
     * @throws FilloException
     * @author Kishan 
     */
    public void gpu_update_data(String sheet, Object data, String fieldvalue, String scenario ) throws FilloException {
        Fillo fillo = new Fillo();
        Connection connection = fillo.getConnection(filepath);
        String strQuery = "update " + sheet + " set "+fieldvalue+"='" + data + "' where id=" + "'" + scenario + "'";
        System.out.println(strQuery);
        connection.executeUpdate(strQuery);
        connection.close();
    }
}
