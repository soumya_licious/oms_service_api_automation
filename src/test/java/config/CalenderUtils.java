package config;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Formatter;

import org.testng.annotations.Test;

public class CalenderUtils {
	
	/**
	 * To get only system Date
	 * format : 2020-03-03
	 * @param calendartype
	 * @param add_substract
	 * @return date
	 */
	public static String getDate(int calendartype, int add_substract){
		Calendar now = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		now.add(calendartype, add_substract);
	    String date  = format.format(now.getTime());
        return date;
    }
	
	
	/**
	 * To get System Date and Time as below format by changing any calendar type
	 * format = " 2020-03-03 16:56:56 "
	 * @param calendartype
	 * @param add_substract
	 * @return date_time
	 */
	public static String getSystemDate_WithTime(int calendartype, int add_substract) 
	{  
		 Calendar now = Calendar.getInstance();
		 SimpleDateFormat timeformat = new SimpleDateFormat("HH:mm:ss");
		 SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
		 now.add(calendartype, add_substract);
		 String time = timeformat.format(now.getTime());
		 String date=dateformat.format(now.getTime());
		 String date_time=date+" "+time;
		 return date_time;
	}  
	
	public static String getSystemDate_YYYYMMDD()
	{
		String date = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
		return date;
	}
	
}
