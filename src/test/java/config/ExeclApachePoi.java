package config;

import java.io.File;
import java.io.FileInputStream;

import java.io.FileOutputStream;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;



import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class ExeclApachePoi {
	
	/**
	 * @author Soumya S H
	 * Description: To read single cell value
	 * @param filepath
	 * @param sheet
	 * @param row1
	 * @param column
	 * @return
	 * @throws Throwable
	 */
	public static String getExeclCellData(String filepath,String sheet, int row1, int column) throws Throwable
	{
		FileInputStream inp = new FileInputStream(filepath); 
	    Workbook wb = WorkbookFactory.create(inp);
	    Sheet sh=wb.getSheet(sheet);
	    Row row=sh.getRow(row1);
	    Cell cel=row.getCell(column);
	    String data=cel.getStringCellValue(); 
	    return data; 
	}
	
	/**
	 * @author Soumya S H
	 * Description: To write into excel to single cell value
	 * @param filepath
	 * @param sheet
	 * @param row1
	 * @param column
	 * @param data
	 * @throws Throwable
	 */
	public static void setExeclCellData(String filepath,String sheet, int row1, int column, String data) throws Throwable
	{
		FileInputStream inp = new FileInputStream(filepath); 
	    Workbook wb = WorkbookFactory.create(inp);
	    Sheet sh=wb.getSheet(sheet);
	    Row row=sh.createRow(row1);
	    Cell cel=row.createCell(column);
	    cel.setCellValue(data);
	    
	    FileOutputStream fileOut = new FileOutputStream(filepath); 
	    wb.write(fileOut); 
	    fileOut.close();
	}
	
	/**
	 * @author Soumya S H
	 * To read complete column
	 * @param filepath
	 * @param Sheet
	 * @param columnNo
	 * @return list<String> 
	 * @throws Throwable
	 */
	
	public static List<String> completeRow(String filepath, String sheet,int columnNo) throws Throwable
	{
		List<String> productcol1=new ArrayList<String>();
		FileInputStream inp = new FileInputStream(filepath); 
	    Workbook wb = WorkbookFactory.create(inp);
	    Sheet sh=wb.getSheet(sheet);
	  /*  get row index*/
	    int rowindex=sh.getLastRowNum();
	    System.out.println("execl rowindex: "+ rowindex);
	    Row row=sh.getRow(0);
	 
	    /*  using loop*/
	    for(int i=1; i<=rowindex; i++)
	    {
	    	row=sh.getRow(i);
	    	productcol1.add(row.getCell(columnNo).getStringCellValue());
	    }
		
	    //System.out.println("execl Product list:" +productcol1); 
	    wb.close();
	    return productcol1;   
	}
	
	
	/**
	 *  @author Soumya S H
	 * Description: To read complete particular column 
	 * @param filepath
	 * @param Sheet
	 * @param columnNo
	 * @return the value of Array of Interger
	 * @throws Throwable
	 */
	public static int[] toReadColumnData(String sFilepath, String sSheet, int coloumn) {
		int sData[] =null;
		try {
			FileInputStream fis = new FileInputStream(sFilepath);
			Workbook wb = (Workbook) WorkbookFactory.create(fis);
			Sheet sht = wb.getSheet(sSheet);
			int iRowNum = sht.getLastRowNum();
			sData = new int[iRowNum];
			System.out.println("length "+sData.length);
			sData = new int[iRowNum];
			for (int i = 1; i <= iRowNum; i++) {
				//System.out.println("value is "+sht.getRow(i).getCell(coloumn).toString());
				sData[i - 1]=(int) sht.getRow(i).getCell(coloumn).getNumericCellValue();

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sData;
	}
	
/**
 * @author Soumya S H
 * Description: Get particular column size of particular row
 * @param sFilepath : file path
 * @param sSheet : sheet name
 * @param rownum : row number 
 * @return : column size of  particular row
 * @throws Throwable
 */
	public static int getColomnSize( String sFilepath, String sSheet, int rownum) throws Throwable
	{
		int colsize=0; 
		FileInputStream fis = new FileInputStream(sFilepath);
		Workbook wb = (Workbook) WorkbookFactory.create(fis);
		Sheet sht = wb.getSheet(sSheet);
		//int iRowNum = sht.getLastRowNum();
		colsize= sht.getRow(rownum).getLastCellNum();
		System.out.println(" column size num: "+ colsize);
		return colsize;
	}
	
	/**
	 * @author Soumya S H
	 * Description: Get particular row of all column data
	 * @param sFilepath : file path
	 * @param sSheet : sheet name
	 * @param rownum : row number 
	 * @return : each column data of  particular row mention in string array format
	 * @throws Throwable
	 */
	
	public static List<String> getAllColumnNames(String sFilepath, String sSheet, int rownum, int startcol)
	{
		
		List <String>lst=new LinkedList<String>();
		try {
			FileInputStream fis = new FileInputStream(sFilepath);
			Workbook wb = (Workbook) WorkbookFactory.create(fis);
			Sheet sht = wb.getSheet(sSheet);
			int icol= sht.getRow(0).getLastCellNum();
	
			for (int i = startcol; i <= icol-1; i++) {
				lst.add(sht.getRow(rownum).getCell(i).getStringCellValue().toUpperCase());
				//System.out.println("tile: " + sht.getRow(0).getCell(i).getStringCellValue());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		//System.out.println("list="+ lst);
		return lst;	
	}
	
	/**
	 * @author Soumya S H
	 * Description: Get Row size 
	 * @param sFilepath : file path
	 * @param sSheet : sheet name 
	 * @return : row size in integer format
	 * @throws Throwable
	 */
		public static int getRowSize( String sFilepath, String sSheet) throws Throwable
		{
			FileInputStream fis = new FileInputStream(sFilepath);
			Workbook wb = (Workbook) WorkbookFactory.create(fis);
			Sheet sht = wb.getSheet(sSheet);
			int iRowNum = sht.getLastRowNum();
			System.out.println(" column size num: "+ iRowNum);
			return iRowNum;
		}
		
		
		/**
		 * @author Soumya S H
		 * Description: To fetch datas from execl
		 * @param sFilepath : file path
		 * @param sSheet : sheet name 
		 * @return : List<Object> which has stored the values from cell
		 * @throws Throwable
		 */
		
		public static List<Object> hubValuesFetch(String sFilepath, String sSheet, int rowstart, int colstart) throws Throwable
		{
			List<Object> value=new LinkedList<Object>();
			try {
			FileInputStream fis = new FileInputStream(sFilepath);	
			Workbook wb = (Workbook) WorkbookFactory.create(fis);
			Sheet sht = wb.getSheet(sSheet);
			int iColNum =sht.getRow(0).getLastCellNum();
			int iRowNum = sht.getLastRowNum();
			System.out.println(" row size num: "+ iRowNum);
			System.out.println(" column  size num: "+ iColNum);
			Row row;
			Cell cel;
			for(int i=rowstart;i<iRowNum; i++)
			{
				for(int j=colstart; j<iColNum; j++)
				{
					 row=sht.getRow(i);
					 cel=row.getCell(j);
					if(row==null)
						value.add(null);
					
					if(cel==null)
						value.add(null);
					
					else if(cel.getCellType()==cel.CELL_TYPE_STRING)
						value.add(sht.getRow(i).getCell(j).getStringCellValue());
					
					else if(cel.getCellType()==cel.CELL_TYPE_NUMERIC)
						value.add((int)sht.getRow(i).getCell(j).getNumericCellValue());
				}
			}
			
			System.out.println("datas added:"+ value);
			System.out.println("list value size"+ value.size());
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			return value;
			
		}
		
		/**
		 *Description: Get all cell value from execl
		 */
	
	public void getAllCellValueofExecl(String filepath)
	{
		filepath="testfiles/Dummy111.xlsx";
		try
        {
            FileInputStream file = new FileInputStream(new File(filepath));
 
            /*Create Workbook instance holding reference to .xlsx file*/
            XSSFWorkbook workbook = new XSSFWorkbook(file);
 
            /*Get first/desired sheet from the workbook*/
            XSSFSheet sheet = workbook.getSheetAt(0);
            	int rowcount=0, columncount=0;
            /*Iterate through each rows one by one*/
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext())
            {
            	rowcount++;
                Row row = rowIterator.next();
                /*For each row, iterate through all the columns*/
                Iterator<Cell> cellIterator = row.cellIterator();
                 
                while (cellIterator.hasNext())
                {
                	columncount++;
                    Cell cell = cellIterator.next();
                    /*Check the cell type and format accordingly*/
                    switch (cell.getCellType())
                    {
                        case Cell.CELL_TYPE_NUMERIC:
                            System.out.print(cell.getNumericCellValue() + "		");
                            break;
                        case Cell.CELL_TYPE_STRING:
                            System.out.print(cell.getStringCellValue() + "		");
                            break;
                    }
                }
                System.out.println("");
            }
            file.close();
            System.out.println(" row count"+rowcount);
            System.out.println("column count"+ columncount);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


  }   
