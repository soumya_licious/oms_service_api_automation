package config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.codoid.products.exception.FilloException;

import helper.BaseTest;
import helper.JdbcConnection;

public class MySqLQueries extends BaseTest{
	
	final static Logger log = Logger.getLogger(MySqLQueries.class.getName());
	
	
	/**
	 * Description: To get Physical_closing stock quantity of all hubs at city level
	 * Query: select sum(physical_closing_stock) from hub_dispatch_plan where dispatch_date='20191127' and product_id='pr_5965d990c954c' and hub_id in ( select  hub_id from hub_master where city_id=1);
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws FilloException 
	 *
	 */
	public static int get_Prod_PhysicalCClosingStockofCity(String dispatch_date, int city_id,String product_id) throws ClassNotFoundException, SQLException, FilloException
	{
		int ctiy_physical_closing=0;
	
		String query="select sum(physical_closing_stock) from licious.hub_dispatch_plan where date(dispatch_date)='"+dispatch_date+"' and product_id='"+product_id+"' and hub_id in ( select  hub_id from hub_master where city_id="+city_id+")";
		ResultSet result = JdbcConnection.dataBaseQueryRead(query);
		while(result.next())
		{
			ctiy_physical_closing=result.getInt(1);
		}

		return ctiy_physical_closing;
	}
	/**
	 * Description: To get actual_closing stock quantity of all hubs at city level
	 * query: select sum(actual_dispatch_stock) from hub_dispatch_plan where date(dispatch_date)='2019-11-27' and product_id='pr_5965d990c954c' and hub_id in ( select  hub_id from hub_master where city_id=1); 
	 * 
	 */
	public static int get_Prod_Actual_DispatchStockofCity(String dispatch_date,int city_id, String product_id)
	{
		int actual_dispatch=0;
		try 
		{
			String query="select sum(actual_dispatch_stock) from licious.hub_dispatch_plan where date(dispatch_date)='"+dispatch_date+"' and product_id='"+product_id+"' and hub_id in ( select  hub_id from hub_master where city_id="+city_id+")";
			ResultSet result = JdbcConnection.dataBaseQueryRead(query);
			while(result.next())
				{
				actual_dispatch=result.getInt(1);
				}
			
		}
		catch( Exception e)
		{
			log.info("Sql Connection or failed to execute licious.hub_dispatch_plan "+e);
		}
		return actual_dispatch;
	}
	
		/**
		 * 
		 * query: SELECT sum(physical_closing_stock) FROM licious.hub_dispatch_plan where date(dispatch_date)='2019-12-11' and product_id='pr_5785b9065d7e1' and 
			hub_id in ('1','2','3','4','5','6','7','8','9','10','11','12','15','46','87','90','96');
		 */
	   //@Test
		//public void get_Prod_Actual_DispatchStockofCluster()
		public static int getsum_Prod_Actual_DispatchStockofCluster(String dispatch_date,String hub_ids, String product_id)
		{
			int actual_dispatch=0;
			try 
			{
				String query="SELECT sum(actual_dispatch_stock) FROM licious.hub_dispatch_plan where date(dispatch_date)='"+dispatch_date+"' and product_id='"+product_id+"' and \r\n" + 
						"hub_id in "+hub_ids+";";
				ResultSet result = JdbcConnection.dataBaseQueryRead(query);
				while(result.next())
					{
					actual_dispatch=result.getInt(1);
					}
			}
			catch( Exception e)
			{
				log.info("Sql Connection or failed to execute licious.hub_dispatch_plan "+e);
			}
			return actual_dispatch;
		}
		
		/**
		 * To get actual dispatch Stock by cluster from Operations data table from Licious DB.
		 * @param dispatch_date
		 * @param hub_ids
		 * @param product_id
		 * @return
		 */
		public static int getsum_Prod_DispatchStock_FromOperationDataTable_ByCluster(String dispatch_date,String hub_ids, String product_id)
		{
			int actual_dispatch=0;
			try 
			{
				String query="SELECT sum(dispatched_quantity) FROM licious.operations_data where `to` in "+hub_ids+" and "
						+ "date(dispatch_id)='"+dispatch_date+"' and sku='"+product_id+"';";
				ResultSet result = JdbcConnection.dataBaseQueryRead(query);
				while(result.next())
					{
					actual_dispatch=result.getInt(1);
					}
			}
			catch( Exception e)
			{
				log.info("Sql Connection or failed to execute licious.hub_dispatch_plan "+e);
			}
			return actual_dispatch;
		}
		
		/**
		 * To get physical_closing value from hub_dispatchPlan
		 * @param dispatch_date
		 * @param hub_ids
		 * @param product_id
		 * @return
		 */
		public static int getsum_Prod_Closing_StockofCluster(String clsoing_date,String hub_ids, String product_id)
		{
			int closing_stock=0;
			try 
			{
				String query="SELECT sum(physical_closing_stock) FROM licious.hub_dispatch_plan where date(dispatch_date)='"+clsoing_date+"' and product_id='"+product_id+"' and \r\n" + 
						"hub_id in "+hub_ids+";";
				ResultSet result = JdbcConnection.dataBaseQueryRead(query);
				while(result.next())
					{
					closing_stock=result.getInt(1);
					}
			}
			catch( Exception e)
			{
				log.info("Sql Connection or failed to execute licious.hub_dispatch_plan "+e);
			}
			return closing_stock;
		}
		
		
		/**
		 * To get physical_closing value from opening_stock table form licious db
		 * @param dispatch_date
		 * @param hub_ids
		 * @param product_id
		 * @return
		 */
		public static int getsum_Prod_ClosingStockFrom_OpeningStockTable_ByCluster(String closing_date,String hub_ids, String product_id)
		{
			int closing_stock=0;
			try 
			{
				String query="SELECT sum(actual_closing) FROM licious.opening_stock where date(created_at)='"+closing_date+"' and"
						+ " product_id='"+product_id+"' and hub_id in "+hub_ids+";";
				ResultSet result = JdbcConnection.dataBaseQueryRead(query);
				while(result.next())
					{
					closing_stock=result.getInt(1);
					}
			}
			catch( Exception e)
			{
				log.info("Sql Connection or failed to execute licious.hub_dispatch_plan "+e);
			}
			return closing_stock;
		}
	
		/**
		 * New Inventory Closing_stock_table
		 * @param closing_date
		 * @param hub_ids
		 * @param product_id
		 * @return
		 */
		public static int get_Prod_ClosingStockFrom_ClosingStockTable_ByCluster(String closing_date,String hub_ids, String product_id)
		{
			int closing_stock=0;
			try 
			{
				String query="Select sum(closing_stock) from inventory.closing_stock_main_outward where `date`='"+closing_date+"' and \r\n" + 
						"item_code='"+product_id+"' and entity_code in "+hub_ids+";";
				ResultSet result = JdbcConnection.dataBaseQueryRead(query);
				while(result.next())
					{
					closing_stock=result.getInt(1);
					}
			}
			catch( Exception e)
			{
				log.info("Sql Connection or failed to execute licious.hub_dispatch_plan "+e);
			}
			return closing_stock;
		}
		/**
		 * 
		 * @param closing_date
		 * @param hub_ids
		 * @param product_id
		 * @return
		 */
		public static int get_Prod_ActualDispatchkFrom_Inventory_MovTable_ByCluster(String dispatch_date,String entity_or_Hub_ids, String product_id, String Src_entity)
		{
			int dispcth_stock=0;
			try 
			{
				String query="select sum(qty) qty from (select sum(quantity) as qty from inventory.inventory_movements where date(outward_timestamp)\r\n" + 
						" = '"+dispatch_date+"' and item_code='"+product_id+"' and source_entity_code = '"+Src_entity+"' and \r\n" + 
						" destination_entity_code in "+entity_or_Hub_ids+"\r\n" + 
						" group by item_code, destination_entity_code) t1;";
				ResultSet result = JdbcConnection.dataBaseQueryRead(query);
				while(result.next())
					{
					dispcth_stock=result.getInt(1);
					}
			}
			catch( Exception e)
			{
				log.info("Sql Connection or failed to execute licious.hub_dispatch_plan "+e);
			}
			return dispcth_stock;
		}
		
		
	/**
	 * Description: to get credential of prod or dev please pass as request the params
	 * @param Dev_Prod_env , please select for Dev as"Dev_Env" and for production as "Prod_Env"
	 * @param select_db , select the db which you want to use like Licious or Inventory to which db you want to connect
	 * @return
	 */
	public static List<Object> get_SQL_Credentials(String environment_DB) 
    { 
		String param=null;
		String prod_user_name=BaseTest.properties.getProperty("username2");
        String prod_password=BaseTest.properties.getProperty("password2");
        String prod_url=BaseTest.properties.getProperty("url2");
        
    	String dev_user_name2=BaseTest.properties.getProperty("username");
        String dev_password2=BaseTest.properties.getProperty("password");
        String dev_url=BaseTest.properties.getProperty("url");
    
        
        if (environment_DB=="Dev_Env")
            return Arrays.asList(dev_user_name2, dev_password2, dev_url);
        
        else if (environment_DB== "Prod_Env")
            return Arrays.asList(prod_user_name, prod_password, prod_url);	
        
        return Arrays.asList(); 
    } 
	
	public static JdbcConnection select_Database_Connection(String environment_DB) throws ClassNotFoundException
	{
		String user_name=null, password=null,url=null;
		List<Object> creds_licious = get_SQL_Credentials(environment_DB);
		if(creds_licious.isEmpty())
			log.info("Please fetch the proper credentials");
		else
		{
			user_name=(String) creds_licious.get(0);
			password=(String) creds_licious.get(1);
			url=(String) creds_licious.get(2);		
		}
		JdbcConnection licious_conn = new JdbcConnection();
		try {
			licious_conn.establishConnectionToJDBC(url, user_name, password);
		} catch (SQLException e) {
			log.info("Sql Connection failed for "+environment_DB+"     "+e);
			e.printStackTrace();
		}
		return licious_conn;
	}
	 
	/**
	 * 
	 * Query: select hub_id, city_id,hub_name from licious.hub_master where is_super_hub_operated='84' and new_inventory_model=0;
	 * @param super_hub_operated
	 * @param new_inventory_flag
	 * @return
	 * @throws FilloException 
	 */
	public static int get_Child_HubsOfSuperHub(int super_hub_operated, int new_inventory_flag) throws FilloException
	{
		List<Object> childhubs=new ArrayList<>();
		try 
		{
			String query="select hub_id, city_id,hub_name from licious.hub_master where is_super_hub_operated="+super_hub_operated+" and "
					+ "new_inventory_model="+new_inventory_flag+";";
			ResultSet result = JdbcConnection.dataBaseQueryRead(query);
			while(result.next())
				{
			    childhubs.add(result.getInt(1));
				}
		}
		catch( Exception e)
		{
			log.info("Sql Connection or Query Error");
		}
		String out=childhubs.toString();
		utils.gpu_update_data("OldInvSuperHubDispatch", childhubs.toString().substring(1,childhubs.toString().length()-1), "Child_Hubs", "TC1");
		return childhubs.size();
	}
	
	
	/**
	 * 
	 * query: UPDATE `licious`.`hub_inventory_positions` SET `stock_units` = '80' WHERE (`product_id` = 'pr_5785b9065d7e1') and (`hub_id` = '84');
	 * @param product_id
	 * @param hub_id
	 * @param live_stock_units
	 */
	public static void update_SuperHub_Old_Inventory(String product_id, int hub_id, int live_stock_units) {
		
		String query="UPDATE `licious`.`hub_inventory_positions` SET `stock_units` = '"+live_stock_units+"' WHERE"
				+ " (`product_id` = '"+product_id+"') and (`hub_id` = '"+hub_id+"');";
		try {
			ResultSet result = JdbcConnection.dataBaseQueryRead(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error("Update Query failed to update Old inventory stock units"+e);
			e.printStackTrace();
		}
	}
	
	
	// INSERT INTO licious.opening_stock (`product_id`, `hub_id`, `stock_units`, `created_at`, `updated_at`,`actual_closing`) VALUES  ( 'pr_57234a4f6f77b', '18','0', '2019-11-24 20:00:00', '2019-11-24 20:00:00','0'),
	/**
	 * 
	 * @param Product_id
	 * @param hub_id
	 * @param stock_units
	 * @param created_at
	 * @param updated_at
	 * @param actual_closing
	 */
	public static void insertintoClosingstockOldInve(String Product_id, String hub_id, String stock_units, String created_at,String updated_at,int actual_closing)
	{
	String query=" INSERT INTO licious.opening_stock (`product_id`, `hub_id`, `stock_units`, `created_at`, `updated_at`, `actual_closing`)\r\n" + 
			"VALUES\r\n" + 
			"    ( '"+Product_id+"', '"+hub_id+"','"+stock_units+"', '"+created_at+"', '"+updated_at+"','"+actual_closing+"');";
	try {
		int a = JdbcConnection.dataBaseQueryUpdate(query);
		if( a>0)  { log.info("Row inserted successfully"); }
	} catch (SQLException e) {
		//e.printStackTrace();
		log.error("Inserting into Opening_stock table failed i.e as closing_stock"+e );
	}	
	}
	
	
	/**
	 * To get live inventory positions for old inventory 
	 * 	SELECT * FROM licious.hub_inventory_positions where hub_id in('16','17','18','24','84') and product_id in('','pr_5785b9065d7e1','','','','','','');
	 * @param hub_ids
	 * @param product_id
	 * @return
	 */
	public static Map<String,List<Inventory>> get_Hub_Inventory_positions_Old(String hub_ids, String product_id) {
		
		List<Integer> hub_id=new ArrayList<>();
		List<Integer> stock_units=new ArrayList<>();
		List<String> product_ids=new ArrayList<>();
		String query="SELECT * FROM licious.hub_inventory_positions where hub_id in("+hub_ids+") and product_id in('"+product_id+"');";
		Map<String,List<Inventory>> inventorymap=new HashMap<String, List<Inventory>>();

		try 
		{
			ResultSet result = JdbcConnection.dataBaseQueryRead(query);
			while(result.next())
				{
				
				hub_id.add(result.getInt(2));
				stock_units.add(result.getInt(4));
				product_ids.add(result.getString(1));
				}
			
			int lenght_of_prdcut_id = 0;
			for(String id : product_ids) {
				if(!inventorymap.containsKey(id)) {
					List<Inventory> list = new ArrayList<>();
					 MySqLQueries.Inventory inventory = new MySqLQueries().new Inventory(hub_id.get(lenght_of_prdcut_id),stock_units.get(lenght_of_prdcut_id)); 
					//Inventory inventory = new Inventory();
					//inventory.setter(hub_id.get(lenght_of_prdcut_id), stock_units.get(lenght_of_prdcut_id));
					list.add(inventory);
					inventorymap.put(id, list);
				}
				else {
					List list = inventorymap.get(id);
					 MySqLQueries.Inventory inventory = new MySqLQueries().new Inventory(hub_id.get(lenght_of_prdcut_id),stock_units.get(lenght_of_prdcut_id)); 
					//inventory.setter(hub_id.get(lenght_of_prdcut_id), stock_units.get(lenght_of_prdcut_id));
					list.add(inventory);
					inventorymap.put(id, list);
				}
				++ lenght_of_prdcut_id ;
			}
			log.info("Live_inventory Positions");
			for(String key : inventorymap.keySet()) {
				List<Inventory> invt = inventorymap.get(key);
				for(Inventory in : invt) {
					log.info("product_id: "+key +"  Hub_id: "+ in.hub_id +" stock_units" + in.stock_units );
				}
			}
		}catch( Exception e)
		{
			log.info("Sql Connection or Query Error");
		}
		return inventorymap;
	}
	
	/**
	 * query: select * from licious.hub_master where is_super_hub_operated=84;
	 * @param super_hub
	 * @return
	 */
	public static List<Integer> get_superhub_operated_childhubs_id(int super_hub)
	{
		List< Integer> childhubid=new ArrayList<>();
		String query="select * from licious.hub_master where is_super_hub_operated="+super_hub+";";
		try
		{
			ResultSet result = JdbcConnection.dataBaseQueryRead(query);
			while(result.next())
				{
				childhubid.add(result.getInt(1));
				}
		}
		catch( Exception e)
		{
			log.info("Sql connection of get superhub_operated_childhubs or querry error ");
		}
		return childhubid;
	}
	
	/**
	 * Query: UPDATE `planning`.`dispatch_plan` SET `disabled` = '1' WHERE (`date` = '2021-08-09');
	 * disabling all the dispatch-ids for current day.
	 */
	public static void disable_All_Dispatchid_Of_Today()
	{
		String date = CalenderUtils.getSystemDate_YYYYMMDD();
		int status=1;
		int update=0;
		String query="UPDATE `planning`.`dispatch_plan` SET `disabled` = '"+status+"' WHERE (`date` = '"+date+"');";
		try {
			update = JdbcConnection.dataBaseQueryUpdate(query);
			log.info("disablled all the disaptches for: "+ date + "rows affected: "+update);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error("Update Query failed to update 'planning.dispatch_plan`"+e);
			e.printStackTrace();
		}
		if (update==0)
		{  log.info("Query Updation did not happen for this date: "+date+" becasue no row exist");	
		}
	}
	
	/**
	 * query: UPDATE `planning`.`dispatch_plan` SET `status` = 'Outward Done' WHERE (`id` = '237');
	 * status are : 'Outward Done', 'Dispatch Initiated','Dispatch Distribution generated','Inward Done'
	 */
	public static void updateMainDispatch_Status(String status,int dispatch_id)
	{
	
		int update=0;
		String query="UPDATE `planning`.`dispatch_plan` SET `status` = '"+status+"' WHERE (`id` = '"+dispatch_id+"');";
		try {
			update = JdbcConnection.dataBaseQueryUpdate(query);
			log.info("dispatch id of: "+ dispatch_id + "status changed to : "+status);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error("Update Query failed to update 'planning.dispatch_plan`"+e);
			e.printStackTrace();
		}
		if (update==0)
		{  log.info("Query Updation did not happen for this dispatch-id: "+dispatch_id+" becasue no row exist");	
		}
		
	}
	
	/**
	 * Query: SELECT * FROM planning.forecast_split where city_id=1 and category_id=1
	 * @param city_id
	 * @param cat_id
	 * return the Hours with percentage in map
	 */
	public static Map<Integer,Float> get_forecastSplit_HourlyPercentage_ByCity(int city_id, int cat_id)
	{
		Map<Integer,Float> hourlysplit=new HashMap<Integer,Float>();
		String query="select * from planning.forecast_split where city_id="+city_id+" and category_id="+cat_id+";";
		try
		{
			ResultSet result = JdbcConnection.dataBaseQueryRead(query);
			while(result.next())
				{
				hourlysplit.put(result.getInt(3),result.getFloat(4));
				}
		}
		catch( Exception e)
		{
			log.info("Sql connection of get superhub_operated_childhubs or querry error ");
		}
		return hourlysplit;	
	}
	
	/**
	 * 
	 * select * from licious.opening_stock where date(created_at)='2020-02-06' and hub_id in ( 16,17,18) and product_id='pr_5785b9065d7e1';
	 * @param hub_ids
	 * @param product_id
	 * @param created_at
	 * @return
	 */

	public static Map<String,List<ClosingStock>> get_closing_stockofHubs_Old(String hub_ids, String product_id, String created_at) {
		
		List<Integer> hub_id=new ArrayList<>();
		List<Integer> actual_closing=new ArrayList<>();
		List<String> product_ids=new ArrayList<>();
		String query="SELECT * FROM licious.opening_stock where date(created_at)='"+created_at+"' and hub_id in("+hub_ids+") and product_id in('"+product_id+"');";
		Map<String,List<ClosingStock>> closingmap=new HashMap<String, List<ClosingStock>>();

		try 
		{
			ResultSet result = JdbcConnection.dataBaseQueryRead(query);
			while(result.next())
				{
				
				hub_id.add(result.getInt(3));
				actual_closing.add(result.getInt(7));
				product_ids.add(result.getString(2));
				}
			
			int lenght_of_prdcut_id = 0;
			for(String id : product_ids) {
				if(!closingmap.containsKey(id)) {
					List<ClosingStock> list = new ArrayList<>();
					 MySqLQueries.ClosingStock inventory = new MySqLQueries().new ClosingStock(hub_id.get(lenght_of_prdcut_id),actual_closing.get(lenght_of_prdcut_id)); 
					list.add(inventory);
					closingmap.put(id, list);
				}
				else {
					List list = closingmap.get(id);
					 MySqLQueries.ClosingStock inventory = new MySqLQueries().new ClosingStock(hub_id.get(lenght_of_prdcut_id),actual_closing.get(lenght_of_prdcut_id)); 
					list.add(inventory);
					closingmap.put(id, list);
				}
				++ lenght_of_prdcut_id ;
			}
			log.info("Closing units from Opening Stock Table");
			for(String key : closingmap.keySet()) {
				List<ClosingStock> invt = closingmap.get(key);
				for(ClosingStock in : invt) {
					log.info("product_id: "+key +"  Hub_id: "+ in.hub_id +" actual_closing" + in.actual_closing );
				}
			}
		}catch( Exception e)
		{
			log.info("'get_closing_stockofHubs_Old' Sql Connection or Query Error");
		}
		return closingmap;
	}
	
	/**
	 * Toget clsoing for single hub : single product
	 * Query: Select * from licious.opening_stock where date(created_at)='2021-08-01' and product_id='pr_5785b9065d7e1' and hub_id=1;
	 * @param hub_ids
	 * @param product_id
	 * @param created_at
	 * @return
	 */
	public static int get_Closing_Of_Hub(int hub_id, String product_id, String created_at)
	{
		int actualclosing=0;
		//String query="Select * from licious.opening_stock where date(created_at)='"+created_at+"' and product_id='"+product_id+"' and hub_id="+hub_id+";";
		String query="select * from licious.opening_stock o where product_id='pr_5785b9065d7e1' and date(created_at)='2021-10-01' and hub_id in (5)";
		try
		{
			ResultSet result = JdbcConnection.dataBaseQueryRead(query);
			System.out.println("read query");
			System.out.println( result.next());
			while(result.next())
				{
				//actualclosing=(result.getInt(7));
				System.out.println(result.getInt(7));
				}
		}
		catch( Exception e)
		{
			log.info("Sql connection of 'get_Closing_Of_Hub' or querry error ");
		}
		
		return actualclosing;
	}
	
	/**
	 * query:UPDATE `licious`.`opening_stock` SET `actual_closing` = '18' WHERE (date(`created_at`) = '2021-08-01') and (`hub_id` = '1')
        and (`product_id`='pr_5785b9065d7e1');
	 * @param hub_id
	 * @param product_id
	 * @param created_at
	 * @param actualvalue
	 */
	public static void update_Closing_of_hub(int hub_id, String product_id, String created_at,int actualvalue)
	{
		int update=0;
		String query="UPDATE `licious`.`opening_stock` SET `actual_closing` = '"+actualvalue+"' WHERE (date(`created_at`) = '"+created_at+"') and (`hub_id` = '"+hub_id+"')\r\n" + 
				"and (`product_id`='"+product_id+"');";
		try {
			update = JdbcConnection.dataBaseQueryUpdate(query);
			log.info("opening_stock for date"+ created_at + "product_id:"+product_id+" & hub-id:"+hub_id+"clsoing value set: "+actualvalue);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error("Update Query failed to update `licious`.`opening_stock"+e);
			e.printStackTrace();
		}
		if (update==0)
		{  log.info("Query Updation did not happen `licious`.`opening_stock for"+created_at+" becasue no row exist");	
		}
		
	}
	
	/**
	 *  To get Sum of Scheduled Orders on hub by product level from Mysql db
	 * select odr.order_id,oit.quantity,oit.pr_name,oit.product_id,odr.sheduled, odr.created_at, odr.status from licious.order_items oit, licious.orders odr
		where odr.order_id = oit.order_id and odr.hub_id = 17 and oit.product_id = 'pr_5785b9065d7e1' and date(odr.sheduled) = '2020-03-10';	
	 * query: select sum(oit.quantity) from licious.order_items oit, licious.orders odr
		where odr.order_id = oit.order_id and odr.hub_id = 17
		and oit.product_id = 'pr_5785b9065d7e1' and date(odr.sheduled) = '2020-03-10';
	 * @param hub_id
	 * @param product_id
	 * @param scheduled_date
	 * @return
	 * @throws FilloException
	 */
	public static int get_Sum_ScheduledOrders_OfHub_ByProduct(int hub_id, String product_id, String scheduled_date) throws FilloException
	{
		int scheduled_Qty=0;
		try 
		{
			String query="select sum(oit.quantity) from licious.order_items oit, licious.orders odr\r\n" + 
					"		where odr.order_id = oit.order_id and odr.hub_id = '"+hub_id+"'\r\n" + 
					"		and oit.product_id = '"+product_id+"' and date(odr.sheduled) = '"+scheduled_date+"';";
			ResultSet result = JdbcConnection.dataBaseQueryRead(query);
			while(result.next())
				{
				scheduled_Qty=(result.getInt(1));
				//System.out.println("hub-id:"+hub_id+"  value.s.o"+result.getInt(1));
				}
		}
		catch( Exception e)
		{
			log.info("Sql Connection or Query Error in getting S.O from orders Table");
		}
		return scheduled_Qty;
	}
	
	/**
	 * 
	 * Query: SELECT sum(order_items.quantity) 
FROM orders INNER JOIN order_items ON orders.order_id=order_items.order_id
where (orders.sheduled='2021-10-02' or date(orders.order_processing_date)='2021-10-02') and 
orders.created_at <'2021-10-01 21:00:00' and orders.status!='Rejected' and
product_id='pr_58d3ae77dcce0' and hub_id in (select hub_id from hub_master where is_super_hub_operated=67) order by orders.created_at;
	 * @throws SQLException
	 */
	public static int get_Orders_CountOnCityLevelOfSuperHub(int superhubId,String productId,String salesDate,String time) {
		{
			int scheduled_Qty=0;
			try 
			{
				/*String query="SELECT sum(order_items.quantity) \r\n" + 
						"FROM licious.orders INNER JOIN licious.order_items ON licious.orders.order_id=licious.order_items.order_id\r\n" + 
						"where (orders.sheduled='"+salesDate+"' or date(orders.order_processing_date)='"+salesDate+"') and \r\n" + 
						"orders.created_at <'"+salesDate+" "+time+"' and orders.status!='Rejected' and\r\n" + 
						"product_id='"+productId+"' and hub_id in (select hub_id from hub_master where is_super_hub_operated="+superhubId+") order by orders.created_at;";
				*/
				String query="SELECT sum(order_items.quantity) \r\n" + 
						"FROM orders INNER JOIN order_items ON orders.order_id=order_items.order_id\r\n" + 
						"where (orders.sheduled<'2021-10-02' or date(orders.order_processing_date)<'2021-10-02') and \r\n" + 
						"orders.created_at <'2021-10-01 21:00:00' and orders.status!='Rejected' and\r\n" + 
						"product_id='pr_5785b9065d7e1' and hub_id in (select hub_id from hub_master where is_super_hub_operated=67) order by orders.created_at;";
				ResultSet result = JdbcConnection.dataBaseQueryRead(query);
				while(result.next())
					{
					scheduled_Qty=(result.getInt(1));
					System.out.println("scheduled_Qty-id:"+scheduled_Qty+"  value.s.o"+result.getInt(1));
					}
			}
			catch( Exception e)
			{
				log.info("Sql Connection or Query Error in getting S.O from orders Table");
			}
			return scheduled_Qty;
		}
		
	}


public class Inventory{
	 int hub_id;
	 int stock_units;
	 
	 Inventory(int hub_id, int stock_units) {
		 this.hub_id = hub_id;
		 this.stock_units = stock_units;
	 }
	 
	 public int getHubId() {
		 return hub_id;
	 }
	 public int getStockUnits() {
		 return stock_units;
	 }
}

public class ClosingStock{
	 public int hub_id;
	 public int actual_closing;
	 
	 ClosingStock(int hub_id, int actual_closing) {
		 this.hub_id = hub_id;
		 this.actual_closing = actual_closing;
	 }
	 
	 public int getHubId() {
		 return hub_id;
	 }
	 public int getactual_closing() {
		 return actual_closing;
	 }
}

public class HubMaster{
	int hub_ids;
	int city_id;
	String hub_names;
	int super_hub;
	int super_hub_operated;
	
	HubMaster( int hub_ids)
	{
		this.hub_ids=hub_ids;
	}
	
	public int getHub_ids() {
		return hub_ids;
	}

	public void setHub_ids(int hub_ids) {
		this.hub_ids = hub_ids;
	}
	
}

	

	/*
	 * 
	 */
	@Test
	public void get_ListOfCityid() throws ClassNotFoundException, SQLException
	{
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con=DriverManager.getConnection("jdbc:mysql://52.77.22.148:3306/licious", "root", "[q9UZ}w4]jta~+eB");
		Statement state=con.createStatement();
		ResultSet set=state.executeQuery("select id from category_master");
		while(set.next())
		{
			System.out.println(set.getInt(1));
		}
		con.close();
		
	}
	
	//@Test
		public void test1() throws ClassNotFoundException, SQLException
		{
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mysql://52.77.22.148:3306/licious", "root", "[q9UZ}w4]jta~+eB");
			Statement state=con.createStatement();
			ResultSet set=state.executeQuery("select id from category_master");
			while(set.next())
			{
				System.out.println(set.getInt(1));
			}
			con.close();
			
		}
		/* @author Soumya S H
		 * @param hub_id
	     * @param product_id
		 * 
		 * select distinct(prom.id),prom.pr_name, hc.cat_id,prom.pr_weight, prom.gross, prom.uom, prom.net, prom.plu_code,
           pm.hub_id, pm.type,pm.no_of_piceces, pm.is_combo,
           pr.base_price, pr.price_gram  ,pr.base_price_after_discount
           from licious.product_master prom 
           join licious.product_merchantdising pm on prom.id = pm.product_id 
           join licious.hub_category_products  hc on prom.id=hc.product_id
           join licious.product_pricing  pr       on prom.id=pr.product_id
           where prom.id='pr_1w6jjmqgvk7' and pm.hub_id=129 and hc.hub_id=129;
          */
		
		public static Map<String , Object> get_Product_UOMNPrice(int hub_id, String product_id)
		{
			Map<String,Object> columns = new HashMap<String, Object>();
			try 
			{
				String query="select distinct(prom.id),prom.pr_name, hc.cat_id,prom.pr_weight, prom.gross, prom.uom, prom.net, prom.plu_code,\r\n" + 
						"           pm.hub_id, pm.type,pm.no_of_piceces, pm.is_combo,\r\n" + 
						"           pr.base_price, pr.price_gram  ,pr.base_price_after_discount\r\n" + 
						"           from licious.product_master prom \r\n" + 
						"           join licious.product_merchantdising pm on prom.id = pm.product_id \r\n" + 
						"           join licious.hub_category_products  hc on prom.id=hc.product_id\r\n" + 
						"           join licious.product_pricing  pr       on prom.id=pr.product_id\r\n" + 
						"           where prom.id='"+product_id+"' and pm.hub_id="+hub_id+" and hc.hub_id="+hub_id+";";
				ResultSet result = JdbcConnection.dataBaseQueryRead(query);
				while(result.next())
					{
					columns.put("product_id",result.getString(1));
					columns.put("pr_name", result.getString(2));
					columns.put("cat_id",result.getInt(3));
					columns.put("pr_weight",result.getString(4));
					columns.put("pr_gross",result.getString(5));
					columns.put("pr_UOM",result.getString(6));
					columns.put("pr_Net",result.getString(7));
					columns.put("pr_plucode",result.getString(8));
					columns.put("pr_hub_id",result.getString(9));
					columns.put("pr_type",result.getString(10));
					columns.put("pr_no_of_pieces",result.getString(11));
					columns.put("pr_is_combo",result.getInt(12));
					columns.put("pr_base_price",result.getInt(13));
					columns.put("pr_price_per_gram",result.getInt(14));
					columns.put("pr_basepriceafter_discount",result.getInt(15));
					}
			}
			catch( Exception e)
			{
				log.info("Sql Connection or Query Error in getting Product-details like price, gram, UOM");
			}
			
			return columns;
		}

}


