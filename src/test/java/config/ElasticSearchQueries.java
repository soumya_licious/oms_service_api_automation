package config;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.Test;

import com.codoid.products.exception.FilloException;

import helper.BaseTest;


public class ElasticSearchQueries extends BaseTest {
	
	static String filepath="data/dataplanfile.xls";
	static ExcelUtils utils=new ExcelUtils(filepath);
	File file;


		//@Test	
		public  void getCIQMatchedCompanyListForSorted1() throws IOException, JSONException {
							
				JSONArray hitsArray;
				//Query query;
				RestClient restClient = RestClient.builder(new HttpHost("52.66.9.219", 9200, "http")).build();	
				
				HttpEntity enty=new NStringEntity("{\"size\":200,"
						+ "\"query\":{\"bool\":{\"filter\":[{\"bool\":"
						+ "{\"adjust_pure_negative\":true,\"must\":[{\"bool\":"
						+ "{\"adjust_pure_negative\":true,\"must\":[{\"match_phrase\":"
						+ "{\"date\":{\"query\":\"2019-07-24\",\"boost\":1,\"slop\":0}}},"
						+ "{\"match_phrase\":{\"city_id\":{\"query\":1,\"boost\":1,\"slop\":0}}},"
						+ "{\"match_phrase\":{\"hub_id\":{\"query\":1,\"boost\":1,\"slop\":0}}},"
						+ "{\"match_phrase\":{\"version\":{\"query\":1,\"boost\":1,\"slop\":0}}},"
						+ "{\"match_phrase\":{\"product_id\":{\"query\":\"pr_57234a4f6f77b\","
						+ "\"boost\":1,\"slop\":0}}}],\"boost\":1}}],\"boost\":1}}],\"adjust_pure_negative\":true,\"boost\":1}}"
						+ ",\"from\":0}\r\n" + 
						"					",ContentType.APPLICATION_JSON);
							
			
				Response response = restClient.performRequest("GET", "/demand-plan/_search",Collections.<String, String>emptyMap(),enty);
				BufferedInputStream br = new BufferedInputStream(response.getEntity().getContent());
				
			//	System.out.println("response::=="+br+"::: end");
				
				String res = "";
				while (br.available()>0) {
					res += (char)br.read();
				
			 	}
				 br.close();
			
				 
			    JSONObject json = new JSONObject(res);
			    System.out.println("jsonres==="+json.toString()+"==end");
			    
				JSONObject hits = json.getJSONObject("hits");
				 hitsArray = hits.getJSONArray("hits");
				ArrayList<Object> coalitionList = new ArrayList<Object>();
				for (int i=0; i<hitsArray.length(); i++) {
					ArrayList<String> fileNameIdArrList = new ArrayList<String>();
				JSONObject h = hitsArray.getJSONObject(i);
				JSONObject sourceJObj = h.getJSONObject("_source");
				
				System.out.println("sourceJObj :"+i+" "+sourceJObj);
				
				}
			}
				/**
				 * To get total day count of logstock in datalake ES
				 * es_Query = http://13.235.163.138:9200/_sql/_explain?sql=select%20*%20from%20%20logstockplan%20where%20created_at=%272019-08-29%27;
				 * @throws IOException
				 * @throws JSONException
				 */
				//@Test
				//public  void es_DataLake_logStockGetCount() throws IOException, JSONException {
				public static int es_DataLake_logStockGetCount() throws IOException, JSONException {

					
					JSONArray hitsArray;
					RestClient restClient = RestClient.builder(new HttpHost("13.235.163.138", 9200, "http")).build();					
					
					HttpEntity entity=new NStringEntity("{\"from\":0,\"size\":200,"
							+ "\"query\":{\"bool\":{\"filter\":[{\"bool\":{\"must\""
							+ ":[{\"match_phrase\":{\"created_at\":{\"query\":\"2019-08-29\",\"slop\":0,"
							+ "\"boost\":1.0}}}],\"adjust_pure_negative\":true,"
							+ "\"boost\":1.0}}],\"adjust_pure_negative\":true,\"boost\":1.0}}}"
						,ContentType.APPLICATION_JSON);
					
				
					Response response = restClient.performRequest("GET", "/logstockplan/_search",Collections.<String, String>emptyMap(),entity);
					BufferedInputStream br = new BufferedInputStream(response.getEntity().getContent());
					
				//	System.out.println("response::=="+br+"::: end");
					
					String res = "";
					while (br.available()>0) {
						res += (char)br.read();
					
				 	}
					 br.close();
									 
				    JSONObject json = new JSONObject(res);				  	    
					JSONObject hits = json.getJSONObject("hits");
					 hitsArray = hits.getJSONArray("hits");
					 
					 int total =hits.getInt("total");
					 System.out.println("total count:: "+total);
					return total;
		}
				
				/**
				 * To get total day count of logstock in Production ES
				 * es_Query = https://plan-es1.licious.app/_sql/_explain?sql=select%20*%20from%20logstockplan%20where%20created_at=%20%272019-08-28%27;
				 * @throws IOException
				 * @throws JSONException
				 */
				//@Test
				//public  void es_Prod_logStockGetCount() throws IOException, JSONException {
				public  static int es_Prod_logStockGetCount() throws IOException, JSONException {
					
					JSONArray hitsArray;
					RestClient restClient = RestClient.builder(new HttpHost("plan-es1.licious.app", 9200, "http")).build();					
					
					HttpEntity entity=new NStringEntity("{\"from\":0,\"size\":200,"
							+ "\"query\":{\"bool\":{\"filter\":"
							+ "[{\"bool\":{\"must\":[{\"match_phrase\":"
							+ "{\"created_at\":{\"query\":\"2019-08-28\",\"slop\":0,\"boost\":1.0}}}],"
							+ "\"adjust_pure_negative\":true,\"boost\":1.0}}],\"adjust_pure_negative\":true,\"boost\":1.0}}}"
						,ContentType.APPLICATION_JSON);
					
				
					Response response = restClient.performRequest("GET", "/logstockplan/_search",Collections.<String, String>emptyMap(),entity);
					BufferedInputStream br = new BufferedInputStream(response.getEntity().getContent());
					
				//	System.out.println("response::=="+br+"::: end");
					
					String res = "";
					while (br.available()>0) {
						res += (char)br.read();
					
				 	}
					 br.close();
									 
				    JSONObject json = new JSONObject(res);				  	    
					JSONObject hits = json.getJSONObject("hits");
					 hitsArray = hits.getJSONArray("hits");
					 
					 int total =hits.getInt("total");
					 System.out.println("total count:: "+total);
					 return total;
		}
				//@Test
				//public  void es_DataLake_ClosingStockGetCount() throws IOException, JSONException {
				public static JSONObject es_DataLake_ClosingStockGetCount(String previousdate) throws IOException, JSONException {
		
					JSONArray hitsArray;
					RestClient restClient = RestClient.builder(new HttpHost("13.235.163.138", 9200, "http")).build();					
					//String previousdate="2019-08-28";
					HttpEntity entity=new NStringEntity("{\"from\":0,\"size\":200,"
							+ "\"query\":{\"bool\":{\"filter\":["
							+ "{\"bool\":{\"must\":[{\"match_phrase\":{\"osdate\":"
							+ "{\"query\":\""+previousdate+"\",\"slop\":0,\"boost\":1.0}}}],"
							+ "\"adjust_pure_negative\":true,\"boost\":1.0}}],"
							+ "\"adjust_pure_negative\":true,\"boost\":1.0}}}"
						,ContentType.APPLICATION_JSON);
					
				
					Response response = restClient.performRequest("GET", "/closingstock/_search",Collections.<String, String>emptyMap(),entity);
					BufferedInputStream br = new BufferedInputStream(response.getEntity().getContent());
					
				//	System.out.println("response::=="+br+"::: end");
					
					String res = "";
					while (br.available()>0) {
						res += (char)br.read();
					
				 	}
					 br.close();
									 
				    JSONObject json = new JSONObject(res);				  	    
					JSONObject hits = json.getJSONObject("hits");
					 hitsArray = hits.getJSONArray("hits");
					 
					 int total =hits.getInt("total");
					 System.out.println("total count:: "+total);
					return json;
				}
				
				@Test
				//public  void es_Prod_ClosingStockGetCount() throws IOException, JSONException {
				public  static JSONObject es_Prod_ClosingStockGetCount(String previousdate) throws IOException, JSONException {
					
					JSONArray hitsArray;
					RestClient restClient = RestClient.builder(new HttpHost("plan-es1.licious.app", 9200, "http")).build();					
					//String previousdate="2019-08-28";
					HttpEntity entity=new NStringEntity("{\"from\":0,\"size\":200,\"query\":"
							+ "{\"bool\":{\"filter\":[{\"bool\":{\"must\":[{\"match_phrase\":"
							+ "{\"osdate\":{\"query\":\""+previousdate+"\",\"slop\":0,\"boost\":1.0}}}],"
							+ "\"adjust_pure_negative\":true,\"boost\":1.0}}],\"adjust_pure_negative\":true,\"boost\":1.0}}}"
						,ContentType.APPLICATION_JSON);
					
				
					Response response = restClient.performRequest("GET", "/closingstock/_search",Collections.<String, String>emptyMap(),entity);
					BufferedInputStream br = new BufferedInputStream(response.getEntity().getContent());
					
				//	System.out.println("response::=="+br+"::: end");
					
					String res = "";
					while (br.available()>0) {
						res += (char)br.read();
					
				 	}
					 br.close();	 
				    JSONObject json = new JSONObject(res);				  	    
					/*JSONObject hits = json.getJSONObject("hits");
					 hitsArray = hits.getJSONArray("hits");
					 
					 int total =hits.getInt("total");
					 System.out.println("total count:: "+json);*/
					return json;
		}
		
		// {"from":0,"size":200,"query":{"bool":{"filter":[{"bool":{"must":[{"bool":{"must":[{"match_phrase":{"osdate":{"query":"2019-09-23","slop":0,"boost":1.0}}},{"match_phrase":{"hub_id":{"query":1,"slop":0,"boost":1.0}}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}}
		 
		/**
		 * Description: To get the data by passing hub_id in production
		 * https://plan-es1.licious.app/_sql?sql=select%20*%20from%20closingstock%20where%20osdate=%272019-09-23%27%20and%20hub_id=1;
		 * @throws IOException 
		 * 
		 */
		//@Test
		//public void es_Prod_ClosingStock_DataCompare() throws IOException
		public static JSONObject es_Prod_ClosingStock_DataCompare(String previousdate, int hub_id) throws IOException
		{
			
			RestClient restClient = RestClient.builder(new HttpHost("plan-es1.licious.app", 9200, "http")).build();					
			//String previousdate="2019-08-28";
			//int hub_id=1;
			HttpEntity entity=new NStringEntity("{\"from\":0,\"size\":200,\"query\":"
					+ "{\"bool\":{\"filter\":[{\"bool\":{\"must\":[{\"bool\":{\"must\":[{\"match_phrase\":"
					+ "{\"osdate\":{\"query\":\""+previousdate+"\",\"slop\":0,\"boost\":1.0}}},"
					+ "{\"match_phrase\":{\"hub_id\":{\"query\":"+hub_id+",\"slop\":0,\"boost\":1.0}}}],"
					+ "\"adjust_pure_negative\":true,\"boost\":1.0}}],\"adjust_pure_negative\":true,\"boost\":1.0}}],"
					+ "\"adjust_pure_negative\":true,\"boost\":1.0}}}"
				,ContentType.APPLICATION_JSON);
			
		
			Response response = restClient.performRequest("GET", "/closingstock/_search",Collections.<String, String>emptyMap(),entity);
			BufferedInputStream br = new BufferedInputStream(response.getEntity().getContent());
						
			String res = "";
			while (br.available()>0) {
				res += (char)br.read();
		 	}
			 br.close();	 
			    JSONObject json = new JSONObject(res);	
			    //System.out.println(">>>>"+json);
				return json;    	    	
		}
		
		/**
		 * Description: To get the data by passing hub_id in DataLake
		 * {"from":0,"size":200,"query":{"bool":{"filter":[{"bool":{"must":[{"bool":{"must":[{"match_phrase":{"osdate":{"query":"2019-09-05","slop":0,"boost":1.0}}},{"match_phrase":{"hub_id":{"query":"8","slop":0,"boost":1.0}}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}}
		 */
		//@Test
		//public void es_DataLake_ClosingStock_DataCompare() throws IOException
		public static JSONObject es_DataLake_ClosingStock_DataCompare(String previousdate, int hub_id) throws IOException
		{
			
			RestClient restClient = RestClient.builder(new HttpHost("plan-es1.licious.app", 9200, "http")).build();					
			//String previousdate="2019-08-28";
			//int hub_id=1;
			HttpEntity entity=new NStringEntity("{\"from\":0,\"size\":200,\"query\":"
					+ "{\"bool\":{\"filter\":[{\"bool\":{\"must\":[{\"bool\":"
					+ "{\"must\":[{\"match_phrase\":{\"osdate\":"
					+ "{\"query\":\""+previousdate+"\",\"slop\":0,\"boost\":1.0}}},"
					+ "{\"match_phrase\":{\"hub_id\":{\"query\":\""+hub_id+"\",\"slop\":0,\"boost\":1.0}}}],"
					+ "\"adjust_pure_negative\":true,\"boost\":1.0}}],"
					+ "\"adjust_pure_negative\":true,\"boost\":1.0}}],\"adjust_pure_negative\":true,\"boost\":1.0}}}"
				,ContentType.APPLICATION_JSON);
			
		
			Response response = restClient.performRequest("GET", "/closingstock/_search",Collections.<String, String>emptyMap(),entity);
			BufferedInputStream br = new BufferedInputStream(response.getEntity().getContent());
			
		//	System.out.println("response::=="+br+"::: end");
			
			String res = "";
			while (br.available()>0) {
				res += (char)br.read();
		 	}
			 br.close();	 
			    JSONObject json = new JSONObject(res);	
			    //System.out.println(">>>>"+json);
			return json;
			
		}
		
		/**
		 * version query= https://plan-es1.licious.app/_sql?sql=select%20max(version)%20from%20demand-plan%20where%20city_id=1%20and%20date=%272019-11-24%27;
		 * version query= {"from":0,"size":0,"query":{"bool":{"filter":[{"bool":{"must":[{"bool":{"must":[{"match_phrase":{"city_id":{"query":1,"slop":0,"boost":1.0}}},{"match_phrase":{"date":{"query":"2019-11-24","slop":0,"boost":1.0}}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}},"_source":{"includes":["MAX"],"excludes":[]},"aggregations":{"maxver":{"max":{"field":"version"}}}}
		 * query = https://plan-es1.licious.app/_sql/?sql=select%20*%20from%20demand-plan%20where%20date=%272019-11-24%27%20%20and%20%20hub_id%20in(%272%27,%277%27,%2712%27)%20and%20product_id=%27pr_hd2jpfd217e%27%20and%20city_id=1%20and%20version=1
		 * query = {"from":0,"size":200,"query":{"bool":{"filter":[{"bool":{"must":[{"bool":{"must":[{"match_phrase":{"date":{"query":"2019-11-24","slop":0,"boost":1.0}}},{"bool":{"should":[{"match_phrase":{"hub_id":{"query":"2","slop":0,"boost":1.0}}},{"match_phrase":{"hub_id":{"query":"7","slop":0,"boost":1.0}}},{"match_phrase":{"hub_id":{"query":"12","slop":0,"boost":1.0}}}],"adjust_pure_negative":true,"boost":1.0}},{"match_phrase":{"product_id":{"query":"pr_hd2jpfd217e","slop":0,"boost":1.0}}},{"match_phrase":{"city_id":{"query":1,"slop":0,"boost":1.0}}},{"match_phrase":{"version":{"query":1,"slop":0,"boost":1.0}}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}}
		 * @throws IOException 
		 * @throws UnsupportedOperationException 
		 */
		//@Test
		//public void get_Prod_Demand() throws IOException
		public static JSONObject get_Prod_Demand_1cluster(String date,String product_id,int hub_id1,int hub_id2, int hub_id3,int city_id) throws UnsupportedOperationException, IOException
		{
			//String date="2019-11-27";
			//int hub_id1=2, hub_id2=7,hub_id3=12, city_id=1;
			//String product_id="pr_hd2jpfd217e";
			    
			int version=getprod_Maxversionof_Demand_By_City_Id(date, city_id);
			
			RestClient restClient = RestClient.builder(new HttpHost("plan-es1.licious.app", 9200, "http")).build();					
			HttpEntity entity=new NStringEntity("{\"from\":0,\"size\":200,\"query\":{\"bool\":{\"filter\":[{\"bool\":"
					+ "{\"must\":[{\"bool\":{\"must\":[{\"match_phrase\":{\"date\":{\"query\":\""+date+"\","
					+ "\"slop\":0,\"boost\":1.0}}},{\"bool\":{\"should\":[{\"match_phrase\":{\"hub_id\":{\"query\":\""+hub_id1+"\","
					+ "\"slop\":0,\"boost\":1.0}}},{\"match_phrase\":{\"hub_id\":{\"query\":\""+hub_id2+"\",\"slop\":0,\"boost\":1.0}}},"
					+ "{\"match_phrase\":{\"hub_id\":{\"query\":\""+hub_id3+"\",\"slop\":0,\"boost\":1.0}}}],\"adjust_pure_negative\":true,"
					+ "\"boost\":1.0}},{\"match_phrase\":{\"product_id\":{\"query\":\""+product_id+"\",\"slop\":0,\"boost\":1.0}}},"
					+ "{\"match_phrase\":{\"city_id\":{\"query\":"+city_id+",\"slop\":0,\"boost\":1.0}}},{\"match_phrase\":{\"version\":"
					+ "{\"query\":"+version+",\"slop\":0,\"boost\":1.0}}}],\"adjust_pure_negative\":true,\"boost\":1.0}}],"
					+ "\"adjust_pure_negative\":true,\"boost\":1.0}}],\"adjust_pure_negative\":true,\"boost\":1.0}}}"
				,ContentType.APPLICATION_JSON);
			
		
			Response response = restClient.performRequest("GET", "/demand-plan/_search",Collections.<String, String>emptyMap(),entity);
			BufferedInputStream br = new BufferedInputStream(response.getEntity().getContent());
			
		//	System.out.println("response::=="+br+"::: end");
			
			String res = "";
			while (br.available()>0) {
				res += (char)br.read();
		 	}
			 br.close();	 
			    JSONObject json = new JSONObject(res);	
			 //   System.out.println(">>>>"+json);
			return json;
			
		}
		
		
		/**
		 * Description: To get demand plan max-version of production elastic search by city-id and date 
		 * https://plan-es1.licious.app/_sql/_explain?sql=select%20max(version)%20%20from%20demand-plan%20where%20city_id=1%20and%20date=%272019-11-27%27;
		 * {"from":0,"size":0,"query":{"bool":{"filter":[{"bool":{"must":[{"bool":{"must":[{"match_phrase":{"city_id":{"query":1,"slop":0,"boost":1.0}}},{"match_phrase":{"date":{"query":"2019-11-27","slop":0,"boost":1.0}}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}},"_source":{"includes":["MAX"],"excludes":[]},"aggregations":{"MAX(version)":{"max":{"field":"version"}}}}
		 * @throws IOException 
		 */
		//@Test
		//public void get_Maxversionof_Demand_By_City_Id() throws IOException
		public static int getprod_Maxversionof_Demand_By_City_Id(String date, int city_id) throws IOException
		{
			//String date="2019-11-27";
			//int city_id=1;
			String demandsearch= BaseTest.properties.getProperty("demand-plan");
			String production_httphost= BaseTest.properties.getProperty("production_httphost");
			RestClient restClientver = RestClient.builder(new HttpHost(production_httphost, 9200, "http")).build();					
			HttpEntity entityver=new NStringEntity("{\"from\":0,\"size\":0,\"query\":{\"bool\":{"
					+ "\"filter\":[{\"bool\":{\"must\":[{\"bool\":{\"must\":[{\"match_phrase\":{\"city_id\":{\"query\":"+city_id+","
					+ "\"slop\":0,\"boost\":1.0}}},{\"match_phrase\":{\"date\":{\"query\":\""+date+"\",\"slop\":0,"
					+ "\"boost\":1.0}}}],\"adjust_pure_negative\":true,\"boost\":1.0}}],\"adjust_pure_negative\":true,"
					+ "\"boost\":1.0}}],\"adjust_pure_negative\":true,\"boost\":1.0}},\"_source\":{\"includes\":[\"MAX\"],"
					+ "\"excludes\":[]},\"aggregations\":{\"MAX(version)\":{\"max\":{\"field\":\"version\"}}}}"
				,ContentType.APPLICATION_JSON);
			Response responsever = restClientver.performRequest("GET", demandsearch,Collections.<String, String>emptyMap(),entityver);
			BufferedInputStream brver = new BufferedInputStream(responsever.getEntity().getContent());
			
			String resver = "";
			while (brver.available()>0) {
				resver += (char)brver.read();
		 	}
			 brver.close();	 
			    JSONObject jsonver = new JSONObject(resver);
			    JSONObject aggregation= jsonver.getJSONObject("aggregations");
			    JSONObject version = aggregation.getJSONObject("MAX(version)");
			    System.out.println("version:  "+version);
			    int getversion=version.getInt("value");
			    //System.out.println("max-version: "+getversion);
			    return getversion;
		}
		
		public static int getdev_Maxversionof_Demand_By_City_Id(String date, int city_id) throws IOException
		{
			//String date="2019-11-27";
			//int city_id=1;
			String demandsearch= BaseTest.properties.getProperty("demand-plan");
			String dev_httphost= BaseTest.properties.getProperty("dev_httphost");
			
			RestClient restClientver = RestClient.builder(new HttpHost(dev_httphost, 9200, "http")).build();					
			HttpEntity entityver=new NStringEntity("{\"from\":0,\"size\":0,\"query\":{\"bool\":{"
					+ "\"filter\":[{\"bool\":{\"must\":[{\"bool\":{\"must\":[{\"match_phrase\":{\"city_id\":{\"query\":"+city_id+","
					+ "\"slop\":0,\"boost\":1.0}}},{\"match_phrase\":{\"date\":{\"query\":\""+date+"\",\"slop\":0,"
					+ "\"boost\":1.0}}}],\"adjust_pure_negative\":true,\"boost\":1.0}}],\"adjust_pure_negative\":true,"
					+ "\"boost\":1.0}}],\"adjust_pure_negative\":true,\"boost\":1.0}},\"_source\":{\"includes\":[\"MAX\"],"
					+ "\"excludes\":[]},\"aggregations\":{\"MAX(version)\":{\"max\":{\"field\":\"version\"}}}}"
				,ContentType.APPLICATION_JSON);
			Response responsever = restClientver.performRequest("GET", demandsearch,Collections.<String, String>emptyMap(),entityver);
			BufferedInputStream brver = new BufferedInputStream(responsever.getEntity().getContent());
			
			String resver = "";
			while (brver.available()>0) {
				resver += (char)brver.read();
		 	}
			 brver.close();	 
			    JSONObject jsonver = new JSONObject(resver);
			    JSONObject aggregation= jsonver.getJSONObject("aggregations");
			    JSONObject version = aggregation.getJSONObject("MAX(version)");
			    boolean t = aggregation.getJSONObject("MAX(version)").isNull("value");
			    if( t== true)
			    {
			    	System.out.println("No demand Plan Found");
			    	return -1;
			    }
			    else {
			    	return aggregation.getJSONObject("MAX(version)").getInt("value");
			    }    	
		}
		
		
		/**
		 * Description: To check whether the product is active or in-active by product-id 
		 * version: {"from":0,"size":0,"query":{"bool":{"filter":[{"bool":{"must":[{"match_phrase":{"product_id":{"query":"pr_57234a4f6f77b","slop":0,"boost":1.0}}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}},"_source":{"includes":["MAX"],"excludes":[]},"aggregations":{"MAX(version)":{"max":{"field":"version"}}}}
		 * version: https://plan-es1.licious.app/_sql?sql=select%20%20max(version)%20from%20product-taxonomy%20where%20product_id=%27pr_57234a4f6f77b%27;
		 * status: https://plan-es1.licious.app/_sql/?sql=select%20%20*%20from%20product-taxonomy%20where%20product_id=%27pr_5785b9065d7e1%27%20and%20version=49%20and%20hub_id=1%20and%20status=1;
		 * status: {"from":0,"size":200,"query":{"bool":{"filter":[{"bool":{"must":[{"bool":{"must":[{"match_phrase":{"product_id":{"query":"pr_5785b9065d7e1","slop":0,"boost":1.0}}},{"match_phrase":{"version":{"query":49,"slop":0,"boost":1.0}}},{"match_phrase":{"hub_id":{"query":1,"slop":0,"boost":1.0}}},{"match_phrase":{"status":{"query":1,"slop":0,"boost":1.0}}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}}
		 * @throws IOException 
		 * @throws FilloException 
		 */
		//@Test
		//public void getProdtaxonomy_active() throws IOException, FilloException
		public static int getProdtaxonomy_active(String product_id, int hub_id) throws IOException
		{
			
			String producttaxonomysearch= BaseTest.properties.getProperty("producttaxonomy");
			String production_httphost= BaseTest.properties.getProperty("production_httphost");
			int getversion=0, status=1, getstatus=0;
			try {
			RestClient restClientver = RestClient.builder(new HttpHost(production_httphost, 9200, "http")).build();					
			HttpEntity entityver=new NStringEntity("{\"from\":0,\"size\":0,\"query\":{\"bool\":{\"filter"
					+ "\":[{\"bool\":{\"must\":[{\"match_phrase\":{\"product_id\":{\"query\":\""+product_id+"\","
					+ "\"slop\":0,\"boost\":1.0}}}],\"adjust_pure_negative\":true,\"boost\":1.0}}],"
					+ "\"adjust_pure_negative\":true,\"boost\":1.0}},\"_source\":{\"includes\":[\"MAX\"],\"excludes\":[]},"
					+ "\"aggregations\":{\"MAX(version)\":{\"max\":{\"field\":\"version\"}}}}"
				,ContentType.APPLICATION_JSON);
			Response responsever = restClientver.performRequest("GET", producttaxonomysearch,Collections.<String, String>emptyMap(),entityver);
			BufferedInputStream brver = new BufferedInputStream(responsever.getEntity().getContent());
			
			String resver = "";
			while (brver.available()>0) {
				resver += (char)brver.read();
		 	}
			 brver.close();	
			 JSONObject json = new JSONObject(resver);	
			 JSONObject aggregation= json.getJSONObject("aggregations");
			 JSONObject version = aggregation.getJSONObject("MAX(version)");
			 getversion=version.getInt("value");
			System.out.println("maxversion of product taxonomy:"+ getversion);
			}
			catch (Exception e)
			{
				System.out.println("version is Zero, invalid");
			}
			
			try {
				RestClient restClientver = RestClient.builder(new HttpHost(production_httphost, 9200, "http")).build();					
				HttpEntity entityver=new NStringEntity("{\"from\":0,\"size\":200,\"query\":{\"bool\":{\"filter\":[{\"bool\":{\"must"
						+ "\":[{\"bool\":{\"must\":[{\"match_phrase\":{\"product_id\":{\"query\":\""+product_id+"\","
						+ "\"slop\":0,\"boost\":1.0}}},{\"match_phrase\":{\"version\":{\"query\":"+getversion+",\"slop\":0,\"boost\":1.0}}},{"
						+ "\"match_phrase\":{\"hub_id\":{\"query\":"+hub_id+",\"slop\":0,\"boost\":1.0}}},{\"match_phrase\":{\"status"
						+ "\":{\"query\":"+status+",\"slop\":0,\"boost\":1.0}}}],\"adjust_pure_negative\":true,\"boost\":1.0}}],"
						+ "\"adjust_pure_negative\":true,\"boost\":1.0}}],\"adjust_pure_negative\":true,\"boost\":1.0}}}"
					,ContentType.APPLICATION_JSON);
				Response responsever = restClientver.performRequest("GET", producttaxonomysearch,Collections.<String, String>emptyMap(),entityver);
				BufferedInputStream brver = new BufferedInputStream(responsever.getEntity().getContent());
				
				String resver = "";
				while (brver.available()>0) {
					resver += (char)brver.read();
			 	}
				 brver.close();	
				 JSONObject json = new JSONObject(resver);	
				 JSONObject hits= json.getJSONObject("hits");
				 JSONArray hitsArray=hits.getJSONArray("hits");
				 JSONObject source = hitsArray.getJSONObject(0);
				 JSONObject sourceJObj = source.getJSONObject("_source");
				 getstatus=sourceJObj.getInt("status");
				}
				catch (Exception e)
				{
					//System.out.println("this product is "+product_id+" inactive");
					getstatus=0;
				}
			return getstatus;
		}
		/**
		 * 
		 * query: https://plan-es1.licious.app/_sql/?sql=select%20sum(final_forecast)%20from%20demand-plan%20where%20date=%272019-11-28%27%20%20%20%20and%20product_id=%27pr_5965d990c954c%27%20and%20city_id=1%20and%20version=1
		 * query: {"from":0,"size":0,"query":{"bool":{"filter":[{"bool":{"must":[{"bool":{"must":[{"match_phrase":{"date":{"query":"2019-11-28","slop":0,"boost":1.0}}},{"match_phrase":{"product_id":{"query":"pr_5965d990c954c","slop":0,"boost":1.0}}},{"match_phrase":{"city_id":{"query":1,"slop":0,"boost":1.0}}},{"match_phrase":{"version":{"query":1,"slop":0,"boost":1.0}}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}},"_source":{"includes":["SUM"],"excludes":[]},"aggregations":{"SUM(final_forecast)":{"sum":{"field":"final_forecast"}}}}
		 * @throws IOException 
		 * 
		 */
		//@Test
		public static int getprod_Sumofdemand_city(String date,int city_id, String product_id) throws IOException
		{
			//String date="2019-11-28";
			//int city_id=1;
			int version=getprod_Maxversionof_Demand_By_City_Id(date, city_id);
			String demandsearch= BaseTest.properties.getProperty("demand-plan");
			String production_httphost= BaseTest.properties.getProperty("production_httphost");
			
			RestClient restClientver = RestClient.builder(new HttpHost(production_httphost, 9200, "http")).build();					
			HttpEntity entityver=new NStringEntity("{\"from\":0,\"size\":0,\"query\":{\"bool\":{\"filter\":[{\"bool\":{\"must\":[{"
					+ "\"bool\":{\"must\":[{\"match_phrase\":{\"date\":{\"query\":\""+date+"\",\"slop\":0,\"boost\":1.0}}},{"
					+ "\"match_phrase\":{\"product_id\":{\"query\":\""+product_id+"\",\"slop\":0,\"boost\":1.0}}},{\"match_phrase\":{"
					+ "\"city_id\":{\"query\":1,\"slop\":0,\"boost\":1.0}}},{\"match_phrase\":{\"version\":{\"query\":"+version+",\"slop\":0,"
					+ "\"boost\":1.0}}}],\"adjust_pure_negative\":true,\"boost\":1.0}}],\"adjust_pure_negative\":true,\"boost\":1.0}}],"
					+ "\"adjust_pure_negative\":true,\"boost\":1.0}},\"_source\":{\"includes\":[\"SUM\"],\"excludes\":[]},\"aggregations"
					+ "\":{\"SUM(final_forecast)\":{\"sum\":{\"field\":\"final_forecast\"}}}}"
				,ContentType.APPLICATION_JSON);
			Response responsever = restClientver.performRequest("GET", demandsearch,Collections.<String, String>emptyMap(),entityver);
			BufferedInputStream brver = new BufferedInputStream(responsever.getEntity().getContent());
			
			String resver = "";
			while (brver.available()>0) {
				resver += (char)brver.read();
		 	}
			 brver.close();	 
			    JSONObject jsonver = new JSONObject(resver);
			    //System.out.println("json=:"+ jsonver);
			    JSONObject aggregation= jsonver.getJSONObject("aggregations");
			    JSONObject sumofFinal = aggregation.getJSONObject("SUM(final_forecast)");
			    int getvalue=sumofFinal.getInt("value");
			    return getvalue;
		}
		
		/**
		 * Description: To get the sum of demand by cluster wise
		 * query: {"from":0,"size":0,"query":{"bool":{"filter":[{"bool":{"must":[{"bool":{"must":[{"match_phrase":{"date":{"query":"2019-12-11","slop":0,"boost":1.0}}},{"match_phrase":{"cluster_id":{"query":"C-1","slop":0,"boost":1.0}}},{"match_phrase":{"product_id":{"query":"pr_hd2jpfd217e","slop":0,"boost":1.0}}},{"match_phrase":{"version":{"query":0,"slop":0,"boost":1.0}}},{"match_phrase":{"city_id":{"query":1,"slop":0,"boost":1.0}}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}},"_source":{"includes":["SUM"],"excludes":[]},"aggregations":{"SUM(final_forecast)":{"sum":{"field":"final_forecast"}}}}
		 * @throws IOException 
		 * 
		 */
		//@Test
		public static int getprod_Sumofdemand_by_Cluster(String date,String cluster,int city_id, String product_id) throws IOException
		{
			//String date="2019-12-11";
			//int city_id=1;
			int version=getprod_Maxversionof_Demand_By_City_Id(date, city_id);
			String demandsearch= BaseTest.properties.getProperty("demand-plan");
			String production_httphost= BaseTest.properties.getProperty("production_httphost");
			
			RestClient restClientver = RestClient.builder(new HttpHost(production_httphost, 9200, "http")).build();					
			HttpEntity entityver=new NStringEntity("{\"from\":0,\"size\":0,\"query\":{\"bool\":{\"filter\":[{\"bool\":{\"must\":[{"
					+ "\"bool\":{\"must\":[{\"match_phrase\":{\"date\":{\"query\":\""+date+"\",\"slop\":0,\"boost\":1.0}}},{"
					+ "\"match_phrase\":{\"cluster_id\":{\"query\":\""+cluster+"\",\"slop\":0,\"boost\":1.0}}},{\"match_phrase\":{\"product_id\":{"
					+ "\"query\":\""+product_id+"\",\"slop\":0,\"boost\":1.0}}},{\"match_phrase\":{\"version\":{\"query\":"+version+",\"slop\":0,"
					+ "\"boost\":1.0}}},{\"match_phrase\":{\"city_id\":{\"query\":"+city_id+",\"slop\":0,\"boost\":1.0}}}],\"adjust_pure_negative"
					+ "\":true,\"boost\":1.0}}],\"adjust_pure_negative\":true,\"boost\":1.0}}],\"adjust_pure_negative\":true,\"boost\":1.0}},"
					+ "\"_source\":{\"includes\":[\"SUM\"],\"excludes\":[]},\"aggregations\":{\"SUM(final_forecast)\":{\"sum\":{\"field\":"
					+ "\"final_forecast\"}}}}"
				,ContentType.APPLICATION_JSON);
			Response responsever = restClientver.performRequest("GET", demandsearch,Collections.<String, String>emptyMap(),entityver);
			BufferedInputStream brver = new BufferedInputStream(responsever.getEntity().getContent());
			
			String resver = "";
			while (brver.available()>0) {
				resver += (char)brver.read();
		 	}
			 brver.close();	 
			    JSONObject jsonver = new JSONObject(resver);
			    //System.out.println("json=:"+ jsonver);
			    JSONObject aggregation= jsonver.getJSONObject("aggregations");
			    JSONObject sumofFinal = aggregation.getJSONObject("SUM(final_forecast)");
			    int getvalue=sumofFinal.getInt("value");
			   // System.out.println("getvalue=:"+ getvalue);
			    return getvalue;
		}
		/**
		 * 
		 *  https://plan-es1.licious.app/_sql?sql=select%20*%20from%20brining-plan-cluster%20where%20production_date=%272020-01-17%27%20%20%20and%20product_id=%27pr_8izjkuohf8n%27%20and%20cluster_id=%27C-11%27%20and%20city_id=11;
		 *  {"from":0,"size":200,"query":{"bool":{"filter":[{"bool":{"must":[{"bool":{"must":[{"match_phrase":{"production_date":{"query":"2020-01-17","slop":0,"boost":1.0}}},{"match_phrase":{"product_id":{"query":"pr_8izjkuohf8n","slop":0,"boost":1.0}}},{"match_phrase":{"cluster_id":{"query":"C-11","slop":0,"boost":1.0}}},{"match_phrase":{"city_id":{"query":11,"slop":0,"boost":1.0}}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}}
		 * @param date
		 * @param cluster
		 * @param city_id
		 * @param product_id
		 * @return
		 * @throws IOException
		 */
		//@Test
		//public void getprod_briningPlan_by_Cluster() throws IOException
		public static int getprod_briningPlan_by_Cluster(String date,String cluster,int city_id, String product_id) throws IOException
		{
		//String date="2020-01-17", product_id="pr_8izjkuohf8n", cluster="C-11";
		//int city_id=11;
		String briningPlansearch= BaseTest.properties.getProperty("brining-plan-cluster");
		String production_httphost= BaseTest.properties.getProperty("production_httphost");
			
		RestClient restClientver = RestClient.builder(new HttpHost(production_httphost, 9200, "http")).build();					
		HttpEntity entityver=new NStringEntity("{\"from\":0,\"size\":200,\"query\":{\"bool\":{\"filter\":"
				+ "[{\"bool\":{\"must\":[{\"bool\":{\"must\":[{\"match_phrase\":{\"production_date\":{\"query\""
				+ ":\""+date+"\",\"slop\":0,\"boost\":1.0}}},{\"match_phrase\":{\"product_id\":{\"query\":\""+product_id+"\",\"slop\""
				+ ":0,\"boost\":1.0}}},{\"match_phrase\":{\"cluster_id\":{\"query\":\""+cluster+"\",\"slop\":0,\"boost\":1.0}}},{\"match_phrase\""
				+ ":{\"city_id\":{\"query\":"+city_id+",\"slop\":0,\"boost\":1.0}}}],\"adjust_pure_negative\":"
				+ "true,\"boost\":1.0}}],\"adjust_pure_negative\":true,\"boost\":1.0}}],\"adjust_pure_negative\":true,\"boost\":1.0}}}"
						,ContentType.APPLICATION_JSON);
		Response responsever = restClientver.performRequest("GET", briningPlansearch,Collections.<String, String>emptyMap(),entityver);
		BufferedInputStream brver = new BufferedInputStream(responsever.getEntity().getContent());
					
		String resver = "";
		while (brver.available()>0) {
				resver += (char)brver.read();
		}
		brver.close();	 
		JSONObject json = new JSONObject(resver);
		//System.out.println("json=:"+ json);
		JSONObject hits= json.getJSONObject("hits");
		JSONArray hitsArray=hits.getJSONArray("hits");
		JSONObject source = hitsArray.getJSONObject(0);
		JSONObject sourceJObj = source.getJSONObject("_source");
		int quantity_to_produce=sourceJObj.getInt("production_actual");
		//System.out.println("quantity_to_produce=:"+ quantity_to_produce);
		return quantity_to_produce;
		}
		
		
		public void getdev_DemandByhubid() {
			
		}
		/**
		 * description: to get the demand-plan of all hubs by city id
		 * query: https://plan-es1.licious.app/_sql?sql=select%20*%20from%20demand-plan%20where%20date=%272020-04-07%27%20and%20product_id=%27pr_3nqjtznab5c%27%20and%20city_id=1%20and%20version=1;
		 * query: {"from":0,"size":200,"query":{"bool":{"filter":[{"bool":{"must":[{"bool":{"must":[{"match_phrase":{"date":{"query":"2020-04-07","slop":0,"boost":1.0}}},{"match_phrase":{"product_id":{"query":"pr_3nqjtznab5c","slop":0,"boost":1.0}}},{"match_phrase":{"city_id":{"query":1,"slop":0,"boost":1.0}}},{"match_phrase":{"version":{"query":1,"slop":0,"boost":1.0}}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}}
		 * @throws IOException 
		 * 
		 */
		//@Test
		public void get_DemandPlanOfHubs_ByCityId() throws IOException
		{
			String date="2020-05-13", product_id="pr_8izjlf6xn0k";
			int city_id=12;
			int version=getdev_Maxversionof_Demand_By_City_Id(date, city_id);
			String demandsearch= BaseTest.properties.getProperty("demand-plan");
			String dev_httphost= BaseTest.properties.getProperty("dev_httphost");
			
			RestClient restClient = RestClient.builder(new HttpHost(dev_httphost, 9200, "http")).build();					
			HttpEntity entity=new NStringEntity("{\"from\":0,\"size\":200,\"query\":{\"bool\":{\"filter\":[{\"bool\":{\"must\":[{\"bool\":"
					+ "{\"must\":[{\"match_phrase\":{\"date\":{\"query\":\""+date+"\",\"slop\":0,\"boost\":1.0}}},{\"match_phrase\":"
					+ "{\"product_id\":{\"query\":\""+product_id+"\",\"slop\":0,\"boost\":1.0}}},{\"match_phrase\":{\"city_id\":"
					+ "{\"query\":"+city_id+",\"slop\":0,\"boost\":1.0}}},{\"match_phrase\":{\"version\":{\"query\":"+version+",\"slop\":0,\"boost\":1.0}}}],\"adjust_pure_negative\":"
					+ "true,\"boost\":1.0}}],\"adjust_pure_negative\":true,\"boost\":1.0}}],\"adjust_pure_negative\":true,\"boost\":1.0}}}"
				,ContentType.APPLICATION_JSON);
			Response response = restClient.performRequest("GET", demandsearch,Collections.<String, String>emptyMap(),entity);
			BufferedInputStream ver = new BufferedInputStream(response.getEntity().getContent());
			
			String res = "";
			while (ver.available()>0) {
				res += (char)ver.read();
		 	}
			 ver.close();	 
				Map< String,Integer> hub_forecast= new HashMap< String,Integer>();

			    JSONObject json = new JSONObject(res);
				JSONObject hits= json.getJSONObject("hits");
				JSONArray hitsArray=hits.getJSONArray("hits");				
				 hitsArray = hits.getJSONArray("hits");
					
					for (int i=0; i<hitsArray.length(); i++) {
						ArrayList<String> fileNameIdArrList = new ArrayList<String>();
						JSONObject h = hitsArray.getJSONObject(i);
						JSONObject sourceJObj = h.getJSONObject("_source");
					// 	System.out.println("sourceJObj :"+i+" "+sourceJObj);
						hub_forecast.put(sourceJObj.getString("hub_id"), sourceJObj.getInt("final_forecast"));
					}
					
					System.out.println("map"+ hub_forecast);
		}
					
		/**
		 * description: to get the demand-plan of all hubs by cities
		 * query: https://plan-es1.licious.app/_sql?sql=select%20*%20from%20demand-plan%20where%20date=%272020-04-07%27%20and%20product_id=%27pr_3nqjtznab5c%27%20and%20city_id=1%20and%20version=1;
		 * query: {"from":0,"size":200,"query":{"bool":{"filter":[{"bool":{"must":[{"bool":{"must":[{"match_phrase":{"date":{"query":"2020-04-07","slop":0,"boost":1.0}}},{"match_phrase":{"product_id":{"query":"pr_3nqjtznab5c","slop":0,"boost":1.0}}},{"match_phrase":{"city_id":{"query":1,"slop":0,"boost":1.0}}},{"match_phrase":{"version":{"query":1,"slop":0,"boost":1.0}}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}}
		 * 
		 * @throws IOException 
		 */
		//@Test
		public void get_DemandPlanOfHubs_ByCityIdtesting() throws IOException
		{
			String date="2019-01-19", product_id="pr_1kejf00n1jt";
			int city_id=3;
			int version=7; //getprod_Maxversionof_Demand_By_City_Id(date, city_id);
			String demandsearch= BaseTest.properties.getProperty("demand-plan");
			String dev_httphost= BaseTest.properties.getProperty("dev_httphost");
			
			RestClient restClient = RestClient.builder(new HttpHost(dev_httphost, 9200, "http")).build();					
			HttpEntity entity=new NStringEntity("{\"from\":0,\"size\":200,\"query\":{\"bool\":{\"filter\":[{\"bool\":{\"must\":[{\"bool\":"
					+ "{\"must\":[{\"match_phrase\":{\"date\":{\"query\":\""+date+"\",\"slop\":0,\"boost\":1.0}}},{\"match_phrase\":"
					+ "{\"product_id\":{\"query\":\""+product_id+"\",\"slop\":0,\"boost\":1.0}}},{\"match_phrase\":{\"city_id\":"
					+ "{\"query\":"+city_id+",\"slop\":0,\"boost\":1.0}}},{\"match_phrase\":{\"version\":{\"query\":"+version+",\"slop\":0,\"boost\":1.0}}}],\"adjust_pure_negative\":"
					+ "true,\"boost\":1.0}}],\"adjust_pure_negative\":true,\"boost\":1.0}}],\"adjust_pure_negative\":true,\"boost\":1.0}}}"
				,ContentType.APPLICATION_JSON);
			Response response = restClient.performRequest("GET", demandsearch,Collections.<String, String>emptyMap(),entity);
			BufferedInputStream ver = new BufferedInputStream(response.getEntity().getContent());
			
			String res = "";
			while (ver.available()>0) {
				res += (char)ver.read();
		 	}
			 ver.close();	 
			    JSONObject json = new JSONObject(res);
			    System.out.println("json=:"+ json);
			    
		System.out.println("--------------------------------------------------");
		
		String dat="2020-04-07", product="pr_3nqjtznab5c";
		int city=1;
		int vers=1;
		
		       RestClient restClientver= RestClient.builder(new HttpHost("plan-es1.licious.app")).build();
				//RestClient restClientver = RestClient.builder(new HttpHost("plan-es1.licious.app", 9200, "http")).build();					

			    HttpEntity entityver=new NStringEntity("{\"from\":0,\"size\":200,\"query\":{\"bool\":{\"filter\":[{\"bool\":{\"must\":[{\"bool\":"
						+ "{\"must\":[{\"match_phrase\":{\"date\":{\"query\":\""+dat+"\",\"slop\":0,\"boost\":1.0}}},{\"match_phrase\":"
						+ "{\"product_id\":{\"query\":\""+product+"\",\"slop\":0,\"boost\":1.0}}},{\"match_phrase\":{\"city_id\":"
						+ "{\"query\":"+city+",\"slop\":0,\"boost\":1.0}}},{\"match_phrase\":{\"version\":{\"query\":"+vers+",\"slop\":0,\"boost\":1.0}}}],\"adjust_pure_negative\":"
						+ "true,\"boost\":1.0}}],\"adjust_pure_negative\":true,\"boost\":1.0}}],\"adjust_pure_negative\":true,\"boost\":1.0}}}"
					,ContentType.APPLICATION_JSON);
				Response responsever = restClientver.performRequest("GET", demandsearch,Collections.<String, String>emptyMap(),entityver);
				BufferedInputStream brver = new BufferedInputStream(responsever.getEntity().getContent());
				
				String resver = "";
				while (brver.available()>0) {
					resver += (char)brver.read();
			 	}
				 brver.close();	 
				    JSONObject jsonver = new JSONObject(resver);
				    System.out.println("json11:"+ jsonver);   		
		}
		
		      
}
