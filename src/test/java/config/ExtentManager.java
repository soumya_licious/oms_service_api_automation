package config;
import com.aventstack.extentreports.ExtentReporter;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Protocol;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ExtentManager {

    private static File dir;
    private static ExtentReports report;
    private static ExtentTest test;
    private static ExtentHtmlReporter htmlReporter;
    private static String filepath = getDateFolder() + "/" + "APIAUTOMATION " + getDate();

    public static ExtentReports GetExtent() {
        if (report != null)
            return report;
        report = new ExtentReports();
        report.attachReporter(getHtmlReporter());
        String path = filepath.replace(".", "");
        return report;
    }

    private static ExtentReporter getHtmlReporter() {
        htmlReporter = new ExtentHtmlReporter(filepath + ".html");
        htmlReporter.config().setDocumentTitle("MerchantKIOS");
        htmlReporter.config().setReportName("MerchantKIOS");
        htmlReporter.config().setEncoding("utf-8");
        htmlReporter.config().setProtocol(Protocol.HTTP);
        return htmlReporter;
    }

    public static ExtentTest createTest(String name, String desc) {
        test = report.createTest(name, desc);
        return test;
    }

    public static String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("YYYY_MM_dd_HH_mm_");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static File getDateFolder() {
        DateFormat dateFromat = new SimpleDateFormat("YYYY_MM_dd");
        Date date = new Date();
        String dateString = dateFromat.format(date);
        dir = new File("./report/" + dateString);
        if (!dir.exists())
            dir.mkdir();
        return dir;
    }

}
