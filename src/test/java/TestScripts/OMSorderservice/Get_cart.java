package TestScripts.OMSorderservice;

import com.codoid.products.exception.FilloException;
import config.PostData_OrderService;
import helper.BaseTest;
import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import org.apache.log4j.Logger;
import org.hamcrest.Matchers;
import org.json.JSONException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class Get_cart extends BaseTest {
    static String filepathdataplan="data/OrderService.xls";
    public static String customer_key,source,Scenario,no_of_items;
    final static Logger log = Logger.getLogger(AddToCart.class.getName());

    /**
     * baseuri is the server, data is driven from config file
     * base path will append to base uri
     * endpoints will append to base path.
     */
    @BeforeClass()
    public void setting_Up_Framework()
    {
        baseuri = properties.getProperty("DEV_BASE_URI_L3") ;
        basepath = properties.getProperty("DEV_BASE_PATH_L2_3") ;
        RestAssured.baseURI =  baseuri ;
        RestAssured.basePath =  basepath ;

        endpoints = properties.getProperty("get_cart_l3") ;
        customer_key="c_jx1qbtd2";
        token="tk_k3pgtx1b";
        source="website";

    }
    /**
     * Add 1 product to cart
     * @throws JSONException
     * @throws FilloException
     * @throws InterruptedException
     */

    public static int get_Cart_l3(String customer_key,String token_l3,String source) throws JSONException, FilloException, InterruptedException{
        log.info("get cart");
        Map<String,String> headers = new HashMap<>();
        headers.put("token",token_l3);
        headers.put("source", source);

        response=getWithSingleQueryParameterWithHearder("http://dev-api.licious.app/cart/v3/get-cart","customer_key",customer_key,headers);
        response.then().log().all();
        int statuscode = response.getStatusCode();
        if (statuscode==200)
        {log.info("customer_key="+customer_key+"has product in cart clear cart before proceeding");}
        else if (statuscode==202)
        {log.info("Cart is already Empty");}
        else
        { log.warn("Something is wrong with get cart API");}
        return statuscode;
        }
}
