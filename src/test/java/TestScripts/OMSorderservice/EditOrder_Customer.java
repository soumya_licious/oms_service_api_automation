package TestScripts.OMSorderservice;

import com.codoid.products.exception.FilloException;
import config.PostData_OrderService;
import helper.BaseTest;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.apache.log4j.Logger;
import org.hamcrest.Matchers;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class EditOrder_Customer extends BaseTest {

    static String filepathdataplan="data/OrderService.xls";
    public static String customer_key,token,source,endpoints1;
    Response responseDependent;
    Map<String,String> headers = new HashMap<>();
    final static Logger log = Logger.getLogger(EditOrder_Customer.class.getName());
    /**
     * baseuri is the server, data is driven from config file
     * base path will append to base uri
     * endpoints will append to base path.
     */
    @BeforeClass()
    public void setting_Up_Framework()
    {
        baseuri = properties.getProperty("DEV_BASE_URI_L2") ;
        basepath = properties.getProperty("DEV_BASE_PATH_L2_5") ;
        RestAssured.baseURI =  baseuri ;
        RestAssured.basePath =  basepath ;

        endpoints = properties.getProperty("partial_reject") ;
        endpoints1=properties.getProperty("partial_reject_confirm");
        //customer_key="c_jx1qbtd2";
        token="atk_j8bz79zl";
        source="dashboard";
    }
    /**
     * Add 1 product to cart
     * @throws JSONException
     * @throws FilloException
     * @throws InterruptedException
     */
    @Test(priority=1, groups = {"1P_CustomerCO"})
    public void edit_OrderOrPartialRejection() throws JSONException, FilloException{
        log.info("get order-id to");
        response=post_for_L2(endpoints,PostData_OrderService.edit_Order_l2().toString(),token);
        response.then()
                .statusCode(201).
                and().
                body("status" , Matchers.equalToIgnoringCase("success"));
       // response.then().log().all();
        log.info("partial edit is done, yet to confirm");
        requestBodyToPartialConfirmation(response);
    }

    @Test(dependsOnMethods = {"edit_OrderOrPartialRejection"})
    public void edit_OrderPartialRejection_Confirmation() throws FilloException {
        log.info("this method is dependent on partial rejection to confirm it.");
        responseDependent=post_for_L2(endpoints1,requestBodyToPartialConfirmation(response).toString(),token);
        responseDependent.then()
                .statusCode(201)
                .and()
                .body("status",Matchers.equalToIgnoringCase("sucess"));
        //responseDependent.then().log().all();
        log.info("partial rejection is done successfully");
    }

    @Test(priority=2, groups = {""})
    public void edit_OrderOrPartialRejectionRemoveProduct() throws JSONException, FilloException{
        log.info("get order-id to");
        response=post_for_L2(endpoints,PostData_OrderService.edit_Order_l2().toString(),token);
        response.then()
                .statusCode(201).
                and().
                body("status" , Matchers.equalToIgnoringCase("success"));
        // response.then().log().all();
        log.info("partial edit is done, yet to confirm");
        requestBodyToPartialConfirmation(response);
    }

    @Test(dependsOnMethods = {"edit_OrderOrPartialRejection"})
    public void edit_OrderPartialRejection_RemoveProductConfirmation() throws FilloException {
        log.info("this method is dependent on partial rejection to confirm it.");
        responseDependent=post_for_L2(endpoints1,requestBodyToPartialConfirmation(response).toString(),token);
        responseDependent.then()
                .statusCode(201)
                .and()
                .body("status",Matchers.equalToIgnoringCase("sucess"));
        //responseDependent.then().log().all();
        log.info("partial rejection is done successfully");
    }



    public static JSONObject requestBodyToPartialConfirmation(Response responsebody){
        JSONObject responseJson = new JSONObject(responsebody.asString());
        JSONArray values = responseJson.getJSONArray("data");
        JSONObject response1=values.getJSONObject(0);
        response1.remove("payment_type");
        System.out.println(response1);
        String[] sa = new String[1];
        sa[0] = "Dummy Instructions";
        response1.put("instruction",sa);
        System.out.println(response1);
        return response1;
    }
}
