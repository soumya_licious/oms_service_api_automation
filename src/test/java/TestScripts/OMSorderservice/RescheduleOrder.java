package TestScripts.OMSorderservice;

import com.codoid.products.exception.FilloException;
import config.ExcelUtils;
import config.PostData_OrderService;
import helper.BaseTest;
import io.restassured.RestAssured;
import org.apache.log4j.Logger;
import org.hamcrest.Matchers;
import org.json.JSONException;
import org.testng.annotations.BeforeClass;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

public class RescheduleOrder extends BaseTest {
    public static String token;
    static String filepathdataplan="data/OrderService.xls";
    static ExcelUtils utils=new ExcelUtils(filepathdataplan);
    static String update_endpoint;
    String scenario; int no_of_products;
    public  static String params = "customer_key";

    final static Logger log = Logger.getLogger(RescheduleOrder.class.getName());

    @BeforeClass
    public void setting_Up_Framework() throws FilloException, ClassNotFoundException
    {
        baseuri = properties.getProperty("DEV_base_uri") ;
        basepath = properties.getProperty("DEV_base_path") ;

        RestAssured.baseURI =  baseuri ;
        RestAssured.basePath =  basepath ;

        endpoints=properties.getProperty("scheduler_create");
        update_endpoint=properties.getProperty("scheduler_update");
        token=properties.getProperty("token");
    }

    public void re_schedule_order_for_today() throws JSONException, FilloException{
        log.info("order rescheduled for T+1 successfully");
        LocalDate today= LocalDate.now();
        String formattedDate = today.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        endpoints = properties.getProperty("reschedule_L2") ;
        response=post_for_L2(endpoints, PostData_OrderService.reschedule(formattedDate,"hub_details",0,4).toString(),token);
        response.then().
                statusCode(200).
                and().
                body("message" , Matchers.equalTo("Order rescheduled"));
        response.then().log().all();
    }

}
