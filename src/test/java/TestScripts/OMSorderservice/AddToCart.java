package TestScripts.OMSorderservice;

import com.aventstack.extentreports.gherkin.model.Scenario;
import com.codoid.products.exception.FilloException;
import helper.BaseTest;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.apache.log4j.Logger;
import org.hamcrest.Matchers;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import config.PostData_OrderService;

import java.util.HashMap;
import java.util.Map;

public class AddToCart extends BaseTest{

    static String filepathdataplan="data/OrderService.xls";
    public static String customer_key,token_l2,token_l3,source,Scenario,no_of_items;
    final static Logger log = Logger.getLogger(AddToCart.class.getName());
    Map<String,String> headers = new HashMap<>();
    /**
     * baseuri is the server, data is driven from config file
     * base path will append to base uri
     * endpoints will append to base path.
     */
    @BeforeClass()
    public void setting_Up_Framework()
    {
        baseuri = properties.getProperty("DEV_BASE_URI_L2") ;
        basepath = properties.getProperty("DEV_BASE_PATH_L2_2") ;
        RestAssured.baseURI =  baseuri ;
        RestAssured.basePath =  basepath ;

        endpoints = properties.getProperty("add_to_cart_l2") ;
        customer_key="c_jx1qbtd2";
        token_l2= properties.getProperty("token_l2");
        token_l3=properties.getProperty("token_l3");
        source="website";
        headers.put("token", token_l2);
        headers.put("source", source);
    }
    /**
     * Add 1 product to cart
     * @throws JSONException
     * @throws FilloException
     * @throws InterruptedException
     */
    @Test(priority=1, groups = {"1P_CustomerCO"})
    public void add_to_cart_l2() throws JSONException, FilloException, InterruptedException{
        log.info("Adding product to the cart ");
        String scenario="TC2";
        response=post_for_L2(endpoints, PostData_OrderService.add_to_cart_l2(scenario).toString(),token_l2);
        response.then().
                statusCode(200).
                and().
                body("status" , Matchers.equalTo("success"));
        response.then().log().all();
        log.info("Add product to the cart successfully");
    }

    /**
     * Adding to cart multiple products
     * Test case 1 with different products list in a cart
     */
    @Test
    public void add_to_CartL2_MultipleProducts() throws FilloException, InterruptedException {
        log.info("Adding multiple products to the cart ");
        Remove_Cart_Item.clear_Cart(customer_key,source,token_l2,token_l3);
        String scenario="TC"; int no_of_items=3; int i=0;
        for(i=0;i<=no_of_items;i++){
            response=post_for_L2(endpoints,PostData_OrderService.add_to_cart_l2MultipleProducts(scenario,i).toString(),token_l2);
            response.then().
                    statusCode(200).
                    and().
                    body("status" , Matchers.equalTo("success"));
            response.then().log().all();
            JSONObject jsonObj=new JSONObject(response.toString());
            log.info("product P"+i+" = "+jsonObj.get("product_id")+"  added to cart successfully");
        }
    }


    public static void addTOCartL2_1ProductGeneric(String scenario,String endpoint,String token_l2) throws FilloException {
        response=post_for_L2(endpoint, PostData_OrderService.add_to_cart_l2(scenario).toString(),token_l2);
        response.then().log().all();
        int statuscode = response.getStatusCode();
        if(statuscode==200){
            log.info("product added to the cart successfully");}
        else
            log.warn("could not add product into Cart", new Throwable("add to cart failed"));
    }
}
