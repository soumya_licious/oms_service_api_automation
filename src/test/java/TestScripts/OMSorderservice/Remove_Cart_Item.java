package TestScripts.OMSorderservice;

import com.codoid.products.exception.FilloException;
import config.PostData_OrderService;
import helper.BaseTest;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.apache.log4j.Logger;
import org.hamcrest.Matchers;
import org.json.JSONException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class Remove_Cart_Item extends BaseTest {

    static String filepathdataplan="data/OrderService.xls";
    public static String customer_key,token_l2,token_l3,source,endpoints1;
    final static Logger log = Logger.getLogger(Remove_Cart_Item.class.getName());
    /**
     * baseuri is the server, data is driven from config file
     * base path will append to base uri
     * endpoints will append to base path.
     */
    @BeforeClass()
    public void setting_Up_Framework()
    {
        baseuri = properties.getProperty("DEV_BASE_URI_L2") ;
        basepath = properties.getProperty("DEV_BASE_PATH_L2_21") ;
        RestAssured.baseURI =  baseuri ;
        RestAssured.basePath =  basepath ;

        endpoints = properties.getProperty("remove_cart_item_l2") ;
        endpoints1= properties.getProperty("clear_cart");
        customer_key="c_jx1qbtd2";
        token_l2= properties.getProperty("token_l2");
        token_l3=properties.getProperty("token_l3");
        source="website";
    }

    @Test(priority=1, groups = {"1p"}) // dependsOnGroups = {"addtocart"}
    public void remove_Cart_Item_l2() throws JSONException, FilloException, InterruptedException{
        log.info("remove cart -delecting 1 product from cart");
        String scenario="TC1";
        Map<String,String> headers = new HashMap<>();
        headers.put("token", token_l2);
        headers.put("source", source);

        response=post_for_L2(endpoints, PostData_OrderService.remove_Cart_Item_l2(scenario).toString(),token);
        response.then().
                statusCode(200).
                and().
                body("status" , Matchers.equalTo("success"));
        response.then().log().all();
        log.info("product is reomved");
    }

    public static void remove_Single_from_Cart(){

    }

    public static void clear_Cart(String customer_key,String source,String token_l2,String token_l3) throws FilloException, InterruptedException {
        int getcartstatus= Get_cart.get_Cart_l3(customer_key,token_l3,source);
        if (getcartstatus==200)
        {
            log.info("Clearing the cart");
            Map<String, Object> headers = new HashMap<>();
            headers.put("token", token_l2);
            Response resp =deletewithheaders(endpoints1,"customer_key",customer_key,headers);
            resp.then().
                    statusCode(200).
                    and().
                    body("status", Matchers.equalTo("success"));
            resp.then().log().all();
            log.info("Now Cart is made Empty");
        }
        log.info("already the Cart is Empty we can proceed to add into cart");
    }
}
