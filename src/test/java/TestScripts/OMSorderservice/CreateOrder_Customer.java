package TestScripts.OMSorderservice;

import com.codoid.products.exception.FilloException;
import config.PostData_OrderService;
import helper.BaseTest;
import io.restassured.RestAssured;
import org.apache.log4j.Logger;
import org.hamcrest.Matchers;
import org.json.JSONException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CreateOrder_Customer extends BaseTest {

    final static Logger log = Logger.getLogger(CreateOrder_Customer.class.getName());

    static String filepathdataplan="data/OrderService.xls";
    public static String customer_key,token,source;
    /**
     * baseuri is the server, data is driven from config file
     * base path will append to base uri
     * endpoints will append to base path.
     */
    @BeforeClass()
    public void setting_Up_Framework()
    {
        baseuri = properties.getProperty("DEV_BASE_URI_L2") ;
        basepath = properties.getProperty("DEV_BASE_PATH_L2_4") ;
        RestAssured.baseURI =  baseuri ;
        RestAssured.basePath =  basepath ;
        token="tk_k4wtwrkp";
    }
    /**
     * Add valid sellable products to cart, product name chicken supreme
     * @throws JSONException
     * @throws FilloException
     */
    @Test(priority=1,groups = {"1P_CustomerCO"})
    public void create_order_l2() throws JSONException, FilloException {
        log.info("Add product to the cart successfully");
        endpoints = properties.getProperty("create_order_l2") ;
        response=post_for_L2(endpoints, PostData_OrderService.create_order_l2().toString(),token);
        response.then().
                statusCode(201).
                and().
                body("status" , Matchers.equalTo("CREATED_WITH_SUCCESS"));
        response.then().log().all();
    }

    public void create_order_l2UnabletofindCart() throws FilloException {
        log.info("Add product to the cart successfully");
        endpoints = properties.getProperty("create_order_l2") ;
        response=post_for_L2(endpoints, PostData_OrderService.create_order_l2().toString(),token);
        response.then().
                statusCode(417).
                and().
                body("status" , Matchers.equalTo("Unable to find cart"));
        response.then().log().all();
    }

    public void create_order_l2Failed() throws FilloException {
        log.info("Add product to the cart successfully");
        endpoints = properties.getProperty("create_order_l2") ;
        response=post_for_L2(endpoints, PostData_OrderService.create_order_l2().toString(),token);
        response.then().
                statusCode(417).
                and().
                body("status" , Matchers.equalTo("Unable to find cart"));
        response.then().log().all();
    }

}
