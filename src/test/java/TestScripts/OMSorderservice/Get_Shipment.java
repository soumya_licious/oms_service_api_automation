package TestScripts.OMSorderservice;

import com.codoid.products.exception.FilloException;
import config.ExcelUtils;
import helper.BaseTest;
import helper.RestUtilities;
import io.restassured.RestAssured;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.logging.Logger;

public class Get_Shipment extends BaseTest {

    final static Logger log = Logger.getLogger(Get_Shipment.class.getName());
    public  static String params = "customer_key";
    HashMap<String, String> map= new HashMap<>();
    HashMap<String, String> headers= new HashMap<>();
    public static String token;
    static String filepathdataplan="data/OrderService.xls";
    static ExcelUtils utils=new ExcelUtils(filepathdataplan);
    static String update_endpoint;
    String scenario; int no_of_products;

    /**
     * baseuri is the server, data is driven from config file
     * base path will append to base uri
     * endpoints will append to base path.
     */
    @BeforeClass()
    public void setting_Up_Framework()
    {
        baseuri = properties.getProperty("DEV_BASE_URI_L3") ;
        basepath = properties.getProperty("DEV_BASE_PATH_L2_3") ;
        RestAssured.baseURI =  baseuri ;
        RestAssured.basePath =  basepath ;
    }
    /**
     * with the customer key , products are being locked for the specific to particular customer.Where live inventory
     * count gets reduced.Locked inventory count increases for particular product added to cart
     * @throws JSONException
     * @throws FilloException
     * @throws InterruptedException
     */
    @Test(priority=1,groups = {"1P_CustomerCO"})
    public void get_shipments_l3() throws JSONException, FilloException, InterruptedException, FilloException {
        log.info("Ship the cart products that are added to cart successfully");
        String data=utils.get_data("create_order", "add_to_cart", "customer_key");
        endpoints = properties.getProperty("get_shipments_l2") ;
        map.put(params, data);
        headers.put("access-token", "tk_k4wtwrkp");
        headers.put("source", "msite");
        //	  map.put("source", "liciousretailpos");
        response = getWithMultipleQueryParametersWithHeader(endpoints, map,headers);
        response.then().statusCode(200);
        int values=getvalue("Total");
        utils.gpu_update_data("create_order", String.valueOf(values), "total","add_to_cart");
        utils.gpu_update_data("create_order", String.valueOf(values), "amount_to_deduct","add_to_cart");
    }
    public static int getvalue(String totalamount){
        int total = 0;
        JSONObject responseJson = new JSONObject(response.getBody().asString());
        JSONObject values = responseJson.getJSONObject("data");
        JSONArray values1 = values.getJSONArray("shipment_summary");
        for(int n=0;n<values1.length();n++){
            JSONObject response4 = values1.getJSONObject(n);
            JSONArray response5 = response4.getJSONArray("shipment_charges");
            for(int o=0;o<response5.length();o++){
                JSONObject resp6= response5.getJSONObject(o);
                if(resp6.getString("attribute").equals(totalamount)){
                    total=resp6.getInt("value");
                }
            }
        }
        return total;
    }
}
