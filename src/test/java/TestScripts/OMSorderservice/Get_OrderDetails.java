package TestScripts.OMSorderservice;


import com.codoid.products.exception.FilloException;
import config.ExcelUtils;
import helper.BaseTest;
import io.restassured.RestAssured;
import org.apache.log4j.Logger;
import org.hamcrest.Matchers;
import org.json.JSONException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;

public class Get_OrderDetails extends BaseTest {

    public static String token;
    static String filepathdataplan="data/OrderService.xls";
    static ExcelUtils utils=new ExcelUtils(filepathdataplan);
    static String update_endpoint;
    String scenario; int no_of_products;
    public  static String params = "customer_key";
    HashMap<String, String> map= new HashMap<>();
    HashMap<String, String> headers= new HashMap<>();

    final static Logger log = Logger.getLogger(Get_OrderDetails.class.getName());

    @BeforeClass
    public void setting_Up_Framework() throws FilloException, ClassNotFoundException
    {
        baseuri = properties.getProperty("CT_base_uri") ;
        basepath = properties.getProperty("CT_base_path") ;

        RestAssured.baseURI =  baseuri ;
        RestAssured.basePath =  basepath ;

        //Conn=new JdbcConnection();
        //Conn=MySqLQueries.select_Database_Connection("Dev_Env");

        endpoints=properties.getProperty("scheduler_create");
        update_endpoint=properties.getProperty("scheduler_update");
        token=properties.getProperty("token");
    }

    /**
     * Update Schedular
     * Scenario : scenario=TC20, no-of-products=1, product-id=''
     */
    @Test(priority=1, groups="order-details")
    public void get_CompleteOrderDetails() throws NumberFormatException, JSONException, FilloException
    {

        String parent_OrderId="";
        log.info("get Order details");
        headers.put("Content-Type","application/json");
        headers.put("Authorization","");
        String parent_OrderId_key=utils.get_data("create_order", "add_to_cart", "parent_orderid");
        endpoints = properties.getProperty("getCompleteOrderDetails") ;

        response=getWithSingleQueryParameterWithHearder(endpoints,parent_OrderId,parent_OrderId_key,headers);
        response.then().statusCode(200)
                .and()
                .body("parent_order_id", Matchers.equalTo(parent_OrderId));
        log.info("Response body:"+ response.getBody());

    }

    @Test( groups="Order_History")
    public void get_HistoryOfOrderBasedOnCustomerKey() throws NumberFormatException, JSONException, FilloException
    {

        String parent_OrderId="";
        log.info("get Order details");
        headers.put("Content-Type","application/json");
        headers.put("Authorization","");
        HashMap<String, String> queryparams= new HashMap<>();
        queryparams.put("limit","10");
        queryparams.put("page","4");
        String parent_OrderId_key=utils.get_data("create_order", "add_to_cart", "parent_orderid");
        endpoints = properties.getProperty("getCompleteOrderDetails") ;

        response=getWithMultipleQueryParametersWithHeader(endpoints,queryparams,headers);
        response.then().statusCode(200)
                .and()
                .body("parent_order_id", Matchers.equalTo(parent_OrderId));
        log.info("Response body:"+ response.getBody());

    }





}
