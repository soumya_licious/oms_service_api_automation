package helper;


import config.ExcelUtils;
import helper.RestUtilities;
import io.restassured.response.Response;

import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.AfterSuite;

import org.testng.annotations.BeforeSuite;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

public class BaseTest extends RestUtilities {

    public ExtentReports report;
    public ExtentTest test;
    public  static Properties properties;
    public static Response response;
    public static String baseuri ;
    public static String basepath ;
    public static String endpoints ;
    public static String filepath;
    public static ExcelUtils utils;
    
    public static JdbcConnection Conn;
    public static String prod_env="Prod_Env";
	public static String dev_env="Dev_Env";
	    
  	File file;
    


  	 @BeforeSuite()
     public void Framework_initialized ()
     {
         // report = ExtentManager.GetExtent();
         properties = new Properties();
         try {
             properties.load(new FileInputStream(new File("./src/test/java/config/Config.properties")));
             PropertyConfigurator.configure("./src/test/java/config/Log4j.properties");
         } catch (Exception ex) {
             ex.printStackTrace();
         }
     }

        @AfterSuite
        public void tearDown ()
        {
//            report.flush();
        }

}

