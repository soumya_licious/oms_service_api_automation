package helper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.testng.annotations.Test;





public class JdbcConnection {
	
	public static Connection connect;
	/**
	 * @author Soumya
	 * Description: To connect to DataBase
	 * @param url
	 * @param username
	 * @param password
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public static void establishConnectionToJDBC( String url, String username, String password) throws SQLException, ClassNotFoundException
	{
		Class.forName("com.mysql.cj.jdbc.Driver");
		connect=DriverManager.getConnection(url, username, password);
		System.out.println("Mysql DB connection established");
	}
	/**
	 * @author Soumya
	 * Description: To Execute SQL Query
	 * @param querystatement
	 * @return
	 * @throws SQLException
	 */
	public static ResultSet dataBaseQueryRead( String querystatement) throws SQLException
	{
		Statement state=connect.createStatement();
		ResultSet set=state.executeQuery(querystatement);
		return set;
	}
	
	/**
	 * @author Soumya
	 * Description: to update sql query
	 * @param querystatement
	 * @return
	 * @throws SQLException
	 */
	public static int dataBaseQueryUpdate( String querystatement) throws SQLException
	{
		Statement state=connect.createStatement();
		int set = state.executeUpdate(querystatement);
		//ResultSet set=state.executeUpdate(querystatement);
		return set;
	}
	/**
	 * @author Soumya
	 * Description: To Disconnect from DataBase
	 * @throws SQLException
	 */
	public static void disconnectToJDBC() throws SQLException
	{
		connect.close();
	}
	
	/**
	 * Description: Sample code To check or verify DataBase Connection
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	@Test
	public void test1() throws ClassNotFoundException, SQLException
	{
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con=DriverManager.getConnection("jdbc:mysql://52.77.22.148:3306", "root", "[q9UZ}w4]jta~+eB");
		Statement state=con.createStatement();
		//ResultSet set=state.executeQuery("select id from licious.category_master");
		ResultSet set=state.executeQuery("select * from licious.opening_stock where product_id='pr_5785b9065d7e1' and date(created_at)='2021-10-01' and hub_id in (select hub_id from licious.hub_master where is_super_hub_operated=67) order by hub_id asc"); 
		while(set.next())
		{
			System.out.println(set.getInt(4));
		}
		con.close();
	}
	
}
