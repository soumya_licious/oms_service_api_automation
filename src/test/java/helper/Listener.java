package helper;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;


import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class Listener  implements ITestListener
{


    private static File dir;
    public static ExtentReports report;
    public static ExtentTest ExtentTest;
    public static ExtentHtmlReporter htmlReporter;
    public static String filepath = getDateFolder() + "/" + "APIAUTOMATION  " +getDate();


    public static String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("YYYY_MM_dd_HH_mm_ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static File getDateFolder() {
        DateFormat dateFromat = new SimpleDateFormat("YYYY_MM_dd");
        Date date = new Date();
        String dateString = dateFromat.format(date);
        dir = new File("/report/" + dateString);
        if (!dir.exists())
            dir.mkdir();
        return dir;
    }



    public void onFinish(ITestContext arg) {
       // report.close();

    }

    public void onStart(ITestContext arg) {

        String createfolder = getDateFolder() + "/" + "APIAUTOMATION" + getDate() ;
        report = new ExtentReports(System.getProperty("user.dir") + createfolder + ".html",false);


    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
        // TODO Auto-generated method stub

    }

    public void onTestFailure(ITestResult result) {

        ExtentTest = report.startTest(result.getName());

        ExtentTest.log(LogStatus.INFO, " Base url: " + BaseTest.baseuri) ;
        ExtentTest.log(LogStatus.INFO, " Resources Url: " + BaseTest.basepath + BaseTest.endpoints);
        ExtentTest.log(LogStatus.INFO, " Status Line : " + BaseTest.response.getStatusLine());
        ExtentTest.log(LogStatus.INFO,
                " Response Time: " + BaseTest.response.getTimeIn(TimeUnit.MILLISECONDS) + " ms");
        ExtentTest.log(LogStatus.INFO,
                "Content-Length: " + BaseTest.response.headers().getValue("Content-Length") + " KB");
        ExtentTest.log(LogStatus.INFO, " Content Type : " + BaseTest.response.contentType());
        ExtentTest.log(LogStatus.INFO, " ResponseBody: " + BaseTest.response.asString().toString());
        ExtentTest.log(LogStatus.INFO, "Response Header Info: " + BaseTest.response.headers().toString());
        ExtentTest.log(LogStatus.FAIL, " Has Failed Test Case " + result.getThrowable());
        ExtentTest.log(LogStatus.FAIL, " Has Failed Test Case " + result.getName());
        report.endTest(ExtentTest);
        report.flush();

    }

    public void onTestSkipped(ITestResult result) {

       /* ExtentTest = Extent.startTest(result.getName());
        ExtentTest.log(LogStatus.SKIP, " Has Skiped Test Case " + result.getName());
        ExtentTest.log(LogStatus.SKIP, " Has Skiped Test Case " + result.getThrowable());
        log.error("Test case execustion Skipped: " + result.getName());
        log.debug(result.toString());
        Extent.endTest(ExtentTest);
        Extent.flush();
        Logs.endTestCase(result.getName());*/

    }

    public void onTestStart(ITestResult result) {
       /* Logs.startTestCase(result.getName());*/

    }

    public void onTestSuccess(ITestResult result) {

        ExtentTest = report.startTest(result.getName());
//      System.out.println(ExtentTest.toString());

        ExtentTest.log(LogStatus.INFO, " Base url: " + BaseTest.baseuri);
        ExtentTest.log(LogStatus.INFO, " Resources Url: " + BaseTest.basepath + BaseTest.endpoints);
        ExtentTest.log(LogStatus.INFO, " Status Line : " + BaseTest.response.getStatusLine());
        ExtentTest.log(LogStatus.INFO,
                " Response Time: " + BaseTest.response.getTimeIn(TimeUnit.MILLISECONDS) + " ms");
        ExtentTest.log(LogStatus.INFO,
                "Content-Length: " + BaseTest.response.headers().getValue("Content-Length") + " KB");
        ExtentTest.log(LogStatus.INFO, " Content Type : " + BaseTest.response.contentType());
        ExtentTest.log(LogStatus.INFO, " ResponseBody: " + BaseTest.response.asString().toString());
        ExtentTest.log(LogStatus.INFO, "Response Header Info: " + BaseTest.response.headers().toString());
        ExtentTest.log(LogStatus.PASS, " Has Pass Test Case " + result.getName());
        report.endTest(ExtentTest);
        report.flush();

    }
}
