package helper;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Date
{

    public static String getCurrentDateandadding6Days()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.setTime(new java.util.Date()); // Now use today date.
        c.add(Calendar.DATE, 6); // Adding 5 days
        String output = sdf.format(c.getTime());
        System.out.println(output);
        return output;


    }
}
