package helper;

import com.codoid.products.exception.FilloException;
import config.ExcelUtils;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;

public class Dataprovider

{
    static String filepath = "data/data.xls";
    ExcelUtils utils = new ExcelUtils(filepath);

    @DataProvider(name = "Barcode")
    public Object[] primescan() throws FilloException
    {
        ArrayList<String> list = new ArrayList<String>();

        for(int i=1; i<51; i++)
        {
            String str1 = utils.get_data("Barcodes", "barcodesforprimescan", "Barcode_" + i);

            list.add(str1);
        }
        return  list.toArray();
    }

    @DataProvider(name = "PC-SH-OUTWARD")
    public Object[] pcshoutward() throws FilloException
    {
        ArrayList<String> list = new ArrayList<String>();

        for(int i=1; i<41; i++)
        {
            String str1 = utils.get_data("Barcodes", "VerifybarcodeafterPC-SHoutward", "Barcode_" + i);

            list.add(str1);
        }
        return  list.toArray();
    }

    @DataProvider(name = "PC-SH-Inward")
    public Object[] pcshinward() throws FilloException
    {
        ArrayList<String> list = new ArrayList<String>();

        for(int i=1; i<41; i++)
        {
            String str1 = utils.get_data("Barcodes", "VerifybarcodeafterPC-SHInward", "Barcode_" + i);

            list.add(str1);
        }
        return  list.toArray();
    }

    @DataProvider(name = "PC-DC-Outward")
    public Object[] pcdcoutward() throws FilloException
    {
        ArrayList<String> list = new ArrayList<String>();

        for(int i=1; i<11; i++)
        {
            String str1 = utils.get_data("Barcodes", "VerifybarcodeafterPC-DCOutward", "Barcode_" + i);

            list.add(str1);
        }
        return  list.toArray();
    }


    @DataProvider(name = "PC-DC-Inward")
    public Object[] pcdcInward() throws FilloException
    {
        ArrayList<String> list = new ArrayList<String>();

        for(int i=1; i<11; i++)
        {
            String str1 = utils.get_data("Barcodes", "VerifybarcodeafterPC-DCInward", "Barcode_" + i);

            list.add(str1);
        }
        return  list.toArray();
    }


    @DataProvider(name = "SH-HSRDC-Outward")
    public Object[] SHTOHSRDCOutward() throws FilloException
    {
        ArrayList<String> list = new ArrayList<String>();

        for(int i=1; i<21; i++)
        {
            String str1 = utils.get_data("Barcodes", "VerifybarcodeafterSH-HSRDCOutward", "Barcode_" + i);

            list.add(str1);
        }
        return  list.toArray();
    }

    @DataProvider(name = "SH-HSRDC-Inward")
    public Object[] SHTOHSRDCInward() throws FilloException
    {
        ArrayList<String> list = new ArrayList<String>();

        for(int i=1; i<21; i++)
        {
            String str1 = utils.get_data("Barcodes", "VerifybarcodeafterSH-HSRDCInward", "Barcode_" + i);

            list.add(str1);
        }
        return  list.toArray();
    }


    @DataProvider(name = "SH-JPNDC-Outward")
    public Object[] SHTOJPNDCOutward() throws FilloException
    {
        ArrayList<String> list = new ArrayList<String>();

        for(int i=1; i<21; i++)
        {
            String str1 = utils.get_data("Barcodes", "VerifybarcodeafterSH-JPNDCOutward", "Barcode_" + i);

            list.add(str1);
        }
        return  list.toArray();
    }


    @DataProvider(name = "SH-JPNDC-Inward")
    public Object[] SHTOJPNDCInward() throws FilloException
    {
        ArrayList<String> list = new ArrayList<String>();

        for(int i=1; i<21; i++)
        {
            String str1 = utils.get_data("Barcodes", "VerifybarcodeafterSH-JPNDCInward", "Barcode_" + i);

            list.add(str1);
        }
        return  list.toArray();
    }


    @DataProvider(name="InvalidBarcodes")
    public Object[] invalidBarcodes() throws FilloException
    {
        ArrayList<String> list = new ArrayList<String>();

        for(int i=1; i<=3; i++)
        {
            String str1 = utils.get_data("Barcodes", "Invalidbarcodes", "Barcode_" + i);

            list.add(str1);
        }
        return  list.toArray();

    }





}
