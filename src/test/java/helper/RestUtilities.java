package helper;

import io.restassured.http.ContentType;

import io.restassured.http.Header;
import io.restassured.response.Response;

import org.apache.log4j.PropertyConfigurator;
import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import com.codoid.products.exception.FilloException;

import config.ExcelUtils;

import java.util.Map;
import java.util.Properties;

import static io.restassured.RestAssured.given;

public class RestUtilities {

    public static String token;
    public static String param = "barCode";
    public static String filepath = "data/data.xls";
    public static String accesstoken ;
    static  ExcelUtils utils=new ExcelUtils(filepath);
	public static Properties properties;
	
    public static void basic_validation(Response resp) {
        Assert.assertFalse(resp.statusCode() == 500,"Response code for the API call is 500");
    }

   public static Response getPathParamWithHeader(String endpoint, String pathparam , String pathvalue, Map<String,String > headers){
    Response response=given()
            .contentType(ContentType.JSON)
            .headers(headers)
            .pathParam(pathparam,pathvalue)
            .when().log().all()
            .get(endpoint);
       response.then().log().ifValidationFails();
       basic_validation(response);
        return response;
    }

   public static Response getWithSingleQueryParameterWithoutHeader(String endpoint , String param , String value ) {
        Response response = given()
                 .contentType(ContentType.JSON)
                 .queryParam(param,value)
                 .when().log().all()
                 .get(endpoint) ;
        response.then().log().all();
        return response;
   }

   // Modified Map <String,String> to <String,object>    # Author Arun SA
   public Response getWithMultipleQueryParametersWithoutHeader(String endpoint , HashMap qureyparam) {
       Response response = given()
               .contentType(ContentType.JSON)
               .queryParams(qureyparam)
               .when().log().all()
               .get(endpoint) ;
       response.then().log().all();
       return response;

   }

    public static Response getWithSingleQueryParameterWithHearder(String endpoint, String param, String value, Map<String, String> headers) {
        Response response = given()
                .contentType(ContentType.JSON)
                .queryParams(param,value)
                .headers(headers)
                .when().log().ifValidationFails()
                .get(endpoint);
        response.then().log().ifValidationFails();
        basic_validation(response);
        return response;
    }

    public Response getWithMultipleQueryParametersWithHeader(String endpoint, Map<String , String> queryparam , Map<String , String> header) {
        Response response = given()
                .contentType(ContentType.JSON)
                .headers(header)
                .queryParams(queryparam)
                .when().log().ifValidationFails()
                .get(endpoint);
        response.then().log().ifValidationFails();
        basic_validation(response);
        return response;
    }
    // Newly added generic method for getWithMultipleHeaders # Author Arun SA
    public Response getWithMultipleheaders(String endpoint, Map<String,Object>  header ) {
        Response response = given()
                .contentType(ContentType.JSON)
                .headers(header)
                .when().log().all()
                .get(endpoint);
        response.then().log().all();
        basic_validation(response);
        return response;
    }

    public Response post(String end_point, Object body){
        Response response = given()
                .contentType(ContentType.JSON)
                .body(body)
                .when().log().ifValidationFails()
                .post(end_point);
        response.then().log().ifValidationFails();
        return response;
    }

    public Response postwithoneheader(String endpoint , Object body)
    {
        Response response = given()
                 .contentType(ContentType.JSON)
                 .header("token","tk_jw1j54rr")
                .body(body)
                .when().log().all()
                .post(endpoint);
        response.then().log().all();
        return response ;
    }


    public Response postwithmultipleheaders(String end_point, Map<String , String> map, Object body){
        Response response = given()
                .contentType(ContentType.JSON)
                .headers(map)
                .body(body)
                .when().log().ifValidationFails()
                .post(end_point);
        response.then().log().ifValidationFails();
        basic_validation(response);
        return response;
    }



    public Response postwithmultipleheadersandmultipleformdata (String endpoint , Map<String ,String> header , Map<String ,String>  formdata)
    {
        Response response = given()
                 .formParams(formdata)
                 .headers(header)
                 .when().log().ifValidationFails()
                .post(endpoint);
        response.then().log().ifValidationFails();
        return response ;


    }

    public Response postwithmultipleformdata (String endpoint , Map<String, String>  map)
    {
        Response response = given()
                .formParams(map)
                .contentType(ContentType.JSON)
                .when().log().all()
                .post(endpoint);
        response.then().log().all();
        return response ;

    }


    public Response putwithjsonrequest(String endpoint, Object body){
        Response response = given()
                .contentType(ContentType.JSON)
                .body(body)
                .when().log().all()
                .put(endpoint);
        response.then().log().all();
        return response ;

    }

    public Response deletewithoneheader(String endpoint , String parameter , String value){
        Response response = given()
                .header("token","tk_jw1j54rr")
                .queryParam(parameter,value)
                .when().log().all()
                .delete(endpoint);
        response.then().log().all();
        return response;
    }

    public void patch(String endpoint, Object body) {
        Response response = given()
                .contentType(ContentType.JSON)
                .contentType(ContentType.URLENC)
                .body(body)
                .when().log().all()
                .patch(endpoint);
        basic_validation(response);
        response.then().log().all();
    }

    /**
     * This is used for HTTP POST for ERP_Supplychain, where bearer token is passed in the header for authorization
     * @param end_point
     * @param body
     * @return Response
     * @author Kishan
     * @throws FilloException 
     * @throws JSONException 
     */
    public static Response post(String end_point, String body) throws JSONException, FilloException{
    	String accesstoken= utils.get_data("accesstoken", "writetoken", "username");
        Response response = given()
                .contentType(ContentType.JSON)
                .header("Authorization","Bearer "+accesstoken).
                body(body)
                .when().log().ifValidationFails()
                .post(end_point);
        response.then().log().ifValidationFails();
        return response;
    }

    /**
     * This is used to HHTP POST for authorization api where this does not require authorisation in the header.
     * @param end_point
     * @param body
     * @return
     * @author Kishan
     */
    public static Response posts(String end_point, Object body){
        Response response = given()
                .contentType(ContentType.JSON)
                .body(body)
                .when().log().ifValidationFails()
                .post(end_point);
        response.then().log().ifValidationFails();
        return response;
    }
    public  Response INvalidGET(String end_point, Object body) throws FilloException{
    	String accesstoken= utils.get_data("accesstoken", "writetoken", "username");
        Response response = given()
                .contentType(ContentType.JSON)
                .header("Authorization","Bearer "+accesstoken).
                body(body)
                .when().log().ifValidationFails()
                .get(end_point);
        response.then().log().ifValidationFails();
        return response;
    }
    
    /**
     * @author Soumya S H
     * @param endpoint
     * @param file
     * @return
     */
    public Response postFileWithOneHeader(String endpoint , File file, String header)
    {
        Response response = given()
                 .header("token",header)
                .multiPart("file", file,"application/JSON")
                .post(endpoint);
        response.then().log().all();
        return response ;
    }
    
    /**
     * @author Soumya S H
     * @param endpoint
     * @param file
     * @return
     */
    public Response postmultipartWithmultipleHeader(String endpoint , File file, Map<String, String> header, Map<String, String> formparam)
    {
        Response response = given()
        		 .headers(header)
                .multiPart("file", file)
                .formParams(formparam)
                .post(endpoint);
        response.then().log().all();
        return response ;
    }
    /**
     * @author Soumya S H
     * @param endpoint
     * @param body 
     * @param header
     * @return
     */
    public Response postwithoneheader(String endpoint , String body, String header)
    {
        Response response = given()
                 .header("Content-Type","application/JSON")
                 .header("token",header)
                .body(body)
                .when().log().all()
                .post(endpoint);
        response.then().log().all();
        return response ;
    }
    

    /**
     * @author Soumya S H
     * @param endpoint
     * @param body 
     * @param header
     * @return
     */
    public Response postwithoutbody(String endpoint , String header)
    {
        Response response = given()
                 .header("Content-Type","application/JSON")
                 .header("token",header)
                .when().log().all()
                .post(endpoint);
        response.then().log().all();
        return response ;
    }
    
    /**
     * @author Soumya S H
     * @param endpoint
     * @param queryparam
     * @param headers
     * @return
     * @throws FilloException
     */
    public Response postQueryParamsAndHeaders(String endpoint , Map<String, String> queryparam, Map<String, String> headers) {
        Response response = given()
                 .contentType(ContentType.JSON)
                 .queryParams(queryparam)
                 .headers(headers)
                 .when().log().all()
                 .when().log().ifValidationFails()
                 .post(endpoint) ;
        response.then().log().all();
        return response;
   }
    
    public Response postbody(String end_point, String body){
        Response response = given()
        		.contentType(ContentType.JSON)
        		.body(body)
                .when().log().all()
                .post(end_point);
        response.then().log().all();
        return response;
    }

    public static Response post_for_L2(String end_point, String body,String accesstoken) throws JSONException, FilloException{
        //String accesstoken= utils.get_data("accesstoken", "L2_Token", "username");
        Response response = given()
                .contentType(ContentType.JSON)
                .header("token",accesstoken).
                body(body)
                .when().log().ifValidationFails()
                .post(end_point);
        response.then().log().ifValidationFails();
        return response;
    }

    public static Response deletewithheaders(String endpoint,String parameter,String value,Map<String, Object> headers){
        Response response = given()
                .contentType(ContentType.JSON)
                .header("token",headers)
                .queryParam(parameter,value)
                .when().log().all()
                .delete(endpoint);
        response.then().log().all();
        return response;
    }

     
}