package helper;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.json.JSONObject;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.ReadPreference;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongoConnection {
	
	public static JSONObject connecttoMongodb(List<ServerAddress> server,String user_name,String auth_database,String data_base, String password, String getcollection, String queryparam)
	{		 
		 char[] pwd =  password.toCharArray();
		 MongoCredential firefly = MongoCredential.createCredential(user_name, auth_database, pwd);
		    
		 List<MongoCredential> auths = new ArrayList<MongoCredential>();
		 auths.add(firefly);
		    
		 MongoClientOptions options = MongoClientOptions.builder()
	                .readPreference(ReadPreference.primaryPreferred())
	                .sslEnabled(true)
	                .maxConnectionIdleTime(6000)
	                .sslEnabled(true)
	                .build();
		    
		 MongoClient mongoClient = new MongoClient(server, auths, options); 
	     /*Accessing the database */
	     MongoDatabase database = mongoClient.getDatabase(data_base);
		 System.out.println("Connected to the database successfully");
	     
	     /*for (String name : database.listCollectionNames()) { 
	          System.out.println(name); 
	     }*/
	     /*Accessing a collection */
	     MongoCollection<Document> collection = database.getCollection(getcollection);
	     
	     FindIterable<Document> query1 = collection.find( Document.parse(queryparam));    
	     JSONObject json=new JSONObject(query1.first().toJson());   
	     mongoClient.close();
	     return json;     
	}
	
	public static JSONObject connecttoMongodb(String mongo_url, String data_base, String getcollection, String queryparam)
	{		 
		ConnectionString connString = new ConnectionString(mongo_url);
			com.mongodb.client.MongoClient mongoClient = MongoClients.create(connString);
			
	     /*Accessing the database */
	     MongoDatabase database = mongoClient.getDatabase(data_base);
		 System.out.println("Connected to the database successfully");
	     
	     /*for (String name : database.listCollectionNames()) { 
	          System.out.println(name); 
	     }*/
	     /*Accessing a collection */
	     MongoCollection<Document> collection = database.getCollection(getcollection);
	     
	     FindIterable<Document> query1 = collection.find( Document.parse(queryparam));    
	     JSONObject json=new JSONObject(query1.first().toJson());   
	     mongoClient.close();
	     return json;     
	}
	
	

}
